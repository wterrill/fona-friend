import { NgModule } from '@angular/core';
import { TestingComponent } from './testing/testing';
import { LoginFormComponent } from './login-form/login-form';
//import { PopoverComponent } from './popover/popover';
@NgModule({
	declarations: 
	[
		TestingComponent,
		LoginFormComponent,
    	//PopoverComponent
	],
	imports: [],
	exports: 
	[
		TestingComponent,
		LoginFormComponent,
    	//PopoverComponent
	]
})
export class ComponentsModule {}
