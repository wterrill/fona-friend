import { Component } from "@angular/core";
import {
  //IonicPage,
  //NavController,
  NavParams,
  ViewController
} from "ionic-angular";

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: "popover",
  templateUrl: "popover.html"
})
export class PopoverComponent {
  text: string;
  items = null;
  header = null;
  submenus = null;
  debug: Boolean = false

  constructor(private viewCtrl: ViewController, public navParams: NavParams) {
    this.items = this.navParams.data.listData;
    this.header = this.navParams.data.header;

    if(this.debug){console.log(this.items);}
    if(this.debug){alert(JSON.stringify(this.items));}

    this.submenus = new Array(this.items.length).fill(0);
  }
  closeOrExpand(item, i) {

    if(this.debug){console.log(this.submenus);}

    if ((item.submenu.length != 0) || false) {
      if (this.submenus[i] === 0) {
        this.submenus[i] = 1;
      } else {
        this.submenus[i] = 0;
      }
    } else {
      this.viewCtrl.dismiss(item);
    }
    
    if(this.debug){console.log(this.submenus)}
  }

  close(item){
    this.viewCtrl.dismiss(item);
  }
}
