import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';  //Popover
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { VideoPlayer } from '@ionic-native/video-player';
import * as firebase from 'firebase';
import { File } from '@ionic-native/file'
import { FileTransfer } from '@ionic-native/file-transfer';
import { IonicStorageModule } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { Camera } from '@ionic-native/camera';  //CameraOptions
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppVersion } from '@ionic-native/app-version';
import { EmailComposer } from '@ionic-native/email-composer';
import { PopoverComponent} from '../components/popover/popover';


//for lazy loading
import { HttpModule } from '@angular/http';
//for extra gestures swipeup swipedown panup pandown
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
//import * as Hammer from 'hammerjs';

export class CustomHammerConfig extends HammerGestureConfig {
    overrides = {
        'press': { time: 500 },  //set press delay for 1/2 second
        "pan": { direction: 30 },
        "swipe": { direction: 30}
    }
}

// export class MCrewHammerConfig extends HammerGestureConfig {
//   public overrides = {
//     //"pan": { direction: 30 },
//     //"press": { time: 300 },
    
//   };
// }

  // Initialize Firebase
  export const config = {
    apiKey: "AIzaSyD40ksJ1eUAjVJPFbPyDJHbzaYTiuooU6A",
    authDomain: "fonaflavor.firebaseapp.com",
    databaseURL: "https://fonaflavor.firebaseio.com",
    projectId: "fonaflavor",
    storageBucket: "fonaflavor.appspot.com",
    messagingSenderId: "292553036086"
  };
  firebase.initializeApp(config);
  

@NgModule({
  declarations: [
    MyApp,
    PopoverComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    YoutubeVideoPlayer,
    VideoPlayer,
    File,
    FileTransfer,
    BarcodeScanner,
    AuthenticationProvider,
    Camera,
    ScreenOrientation,
    AppVersion,
    EmailComposer,
    {provide: HAMMER_GESTURE_CONFIG, useClass: CustomHammerConfig}
  ]
})
export class AppModule {}
