import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailRequestPage } from './email-request';

@NgModule({
  declarations: [
    EmailRequestPage,
  ],
  imports: [
    IonicPageModule.forChild(EmailRequestPage)
  ],
})
export class FeedbackPageModule {}
