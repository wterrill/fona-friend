import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-email-request',
  templateUrl: 'email-request.html',
})
export class EmailRequestPage {

  constructor(
    //private navCtrl: NavController,
    //private navParams: NavParams,
    private emailComposer: EmailComposer,
    private auth: AuthenticationProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }


sendIt(){
  let email = {
    to: 'sjenkins@FONA.com',
    cc: ['william.terrill@gmail.com', 'jellis@fona.com', '2FonaFriend@gmail.com'],
    //cc: ['john@doe.com', 'jane@doe.com'],
    attachments: [
      // 'file://img/logo.png',
      // 'res://icon.png',
      // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
      // 'file://README.pdf'
    ],
    subject: 'Request Access to Fona Friend Search from user: '+this.auth.currentUser.email,
    body: 'I would like to request access to the Search Function for Fona Friend. \nMy name is:' + this.auth.currentUserData.displayName 
    + '\n and I work for: ' + this.auth.currentUserData.company
    + '\n and I can be reached at this number:' + this.auth.currentUserData.telephone
    + '\n\n Additional Comments:',
    isHtml: false
  };
  
  // Send a text message using default options
  this.emailComposer.open(email);

}


}



