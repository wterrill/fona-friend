import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular'

/**
 * Generated class for the FlavorInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-flavor-info-slide',
  templateUrl: 'flavor-info-slide.html',
})
export class FlavorInfoSlidePage {
  private flavor = null;
  @ViewChild(Slides) slides: Slides;
  private highlight = null;
  private borderColor = null;
  //private favoriteAdd = [];

  //private enteredIsFavorite = null;
  private resultsArray = null;
  private favorites = null;
  private name = null;
  private index = 0;
  private currentIndex = null;
  private heartColor = null;
  private fromFavorite = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private emailComposer: EmailComposer,
    private toastCtrl: ToastController,
    private auth: AuthenticationProvider
  ) {
    this.resultsArray = navParams.get('resultsArray')
    this.index = navParams.get('index');
    this.currentIndex = this.index;
    this.flavor = navParams.get('flavor')
    this.favorites = navParams.get('favorites');
    this.fromFavorite = navParams.get('fromFavorite')
    //this.enteredIsFavorite = this.isFavorite;
    this.highlight = this.flavor.PROFILE_COLOR
    this.name = this.flavor.FLAVOR_NAME;
    this.updateBorder()
    this.heartColor = "red"

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
    //this.slides.slideTo(this.index,300)
  }

  ionViewDidEnter() {
    this.slides.slideTo(this.index, 0)
  }
  slideChanged() {
    this.currentIndex = this.slides.getActiveIndex();
    console.log('Current index is', this.currentIndex);
    if (this.currentIndex < this.resultsArray.length) {
      this.highlight = this.resultsArray[this.currentIndex].PROFILE_COLOR
      this.name = this.resultsArray[this.currentIndex].FLAVOR_NAME
      this.updateBorder()
    }
  }

  updateBorder() {
    switch (this.highlight) {
      case "Berry": {
        this.borderColor = "#D51269";
        break;
      }
      case "Brown": {
        this.borderColor = "#563D0D";
        break;
      }
      case "Melon": {
        this.borderColor = "#74BD4C";
        break;
      }
      case "Fantasy": {
        this.borderColor = "#127BBF";
        break;
      }
      case "Mint": {
        this.borderColor = "#74CDE2";
        break;
      }
      case "Spice": {
        this.borderColor = "#89060D";
        break;
      }
      case "Citrus": {
        this.borderColor = "#FAB231";
        break;
      }
      case "Orchard": {
        this.borderColor = "#C21734";
        break;
      }
      case "Grape": {
        this.borderColor = "purple";
        break;
      }
      case "Herbal": {
        this.borderColor = "#596F57";
        break;
      }
      case "Tropical": {
        this.borderColor = "#F38231";
        break;
      }
      case "Modifier": {
        this.borderColor = "#6D6E71";
        break;
      }
      case "Stone_Fruit": {
        this.borderColor = "darksalmon";
        break;
      }
      default:
        this.borderColor = "0xFFF"
    }
  }

  dismiss() {

    this.viewCtrl.dismiss(this.favorites);

    // this.viewCtrl.dismiss()

  }

  sendItFlavor(flavor) {
    let email = {
      to: '2FonaFriend@gmail.com',
      cc: ['william.terrill@gmail.com', 'jellis@fona.com'],
      //cc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf'
      ],
      subject: 'Flavor Info: ' + flavor["PRODUCT_NAME"] + ' from user: ' + this.auth.currentUser.email,
      body: "The flavor you\'d like information is shown below.  What question do you have?: \n\n\n\n\n\n\n" +
        "Name: " + flavor["PRODUCT_NAME"] + "\n" +
        "Flavor Profile: " + flavor["FLAVOR_PROFILE"] + "\n" +
        "Form: " + flavor.FORM + "\n" +
        "Descriptors: " + flavor.DESCRIPTORS + "\n" +
        "FONA Product Code: " + flavor["FONA_PRODUCT_CODE"] + "\n",
      // "SAP Descriptor: " + flavor["SAP DESCRIPTION"] + "\n" +
      // "SAP Number: " + flavor.SAP_num + "\n" +
      // "Shipped To: " + flavor["SHIPPED TO"] + "\n" +
      // "Last Sold: " + flavor["LAST SOLD*"] + "\n",
      isHtml: false
    };
    // Send a text message using default options
    this.emailComposer.open(email);
  }

  favoriteFlavor(flavor) {
    this.resultsArray[this.currentIndex].isFavorite = true;
    var addit = true;
    //check if it's alreay in favorites
    this.favorites.map(fav => {
      if (flavor.ID == fav.ID) {
        addit = false;
      }
    })
    if (addit) {
      this.favorites.push(JSON.parse(JSON.stringify(flavor)))
    }
    this.presentFavoritedToast()
    console.log("after fav. favorite List:", this.favorites)
  }

  unfavoriteFlavor(flavor) {
    this.resultsArray[this.currentIndex].isFavorite = false;
    var index = this.favorites.findIndex(obj => {
      return obj["ID"] === flavor.ID;
    })
    this.favorites.splice(index, 1);
    console.log("after unfav. Favorites=", this.favorites)
    this.presentUnfavoritedToast()
    if(this.fromFavorite && this.favorites.length < 1){
      this.dismiss();
    }
    if(this.currentIndex == this.resultsArray.length){
      this.currentIndex = this.currentIndex - 1;
      this.slideChanged();
      this.slides.slidePrev();
    } else 
    if(this.fromFavorite && this.resultsArray.length > 0){
      this.highlight=this.resultsArray[this.currentIndex].FLAVOR_PROFILE
      this.name=this.resultsArray[this.currentIndex].FLAVOR_NAME
      this.updateBorder();
    }

  }

  presentFavoritedToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully added to your favorites',
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  presentUnfavoritedToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully removed from your favorites',
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }


}
