import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlavorInfoSlidePage } from './flavor-info-slide';

@NgModule({
  declarations: [
    FlavorInfoSlidePage,
  ],
  imports: [
    IonicPageModule.forChild(FlavorInfoSlidePage),
  ],
})
export class FlavorInfoPageModule {}
