import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlavorInfoPage } from './flavor-info';

@NgModule({
  declarations: [
    FlavorInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(FlavorInfoPage),
  ],
})
export class FlavorInfoPageModule {}
