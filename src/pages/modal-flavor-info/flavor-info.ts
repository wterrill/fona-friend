import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the FlavorInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-flavor-info',
  templateUrl: 'flavor-info.html',
})
export class FlavorInfoPage {
  private flavor = null;
  private highlight = null;
  private favorited = null;
  private isFavorite = null;
  private flavorFlip = false;
  private enteredIsFavorite = null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private emailComposer: EmailComposer,
    private toastCtrl: ToastController,
    private auth: AuthenticationProvider
  ) {
    this.flavor = navParams.get('flavorID')
    this.isFavorite = navParams.get('isFavorite')
    this.enteredIsFavorite = this.isFavorite;
    console.log(this.isFavorite);
    this.highlight = this.flavor.PROFILE_COLOR

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
  }

  dismiss() {

    if(this.enteredIsFavorite != this.isFavorite){
    this.viewCtrl.dismiss(this.favorited);
    } 
    if(this.enteredIsFavorite == this.isFavorite) {
      this.viewCtrl.dismiss()
    }
  }

  sendItFlavor(flavor) {
    let email = {
      to: '2FonaFriend@gmail.com',
      cc: ['william.terrill@gmail.com', 'jellis@fona.com'],
      //cc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf'
      ],
      subject: 'Flavor Info: ' + flavor["PRODUCT_NAME"] + ' from user: ' + this.auth.currentUser.email,
      body: "The flavor you\'d like information is shown below.  What question do you have?: \n\n\n\n\n\n\n" +
        "Name: " + flavor["PRODUCT_NAME"] + "\n" +
        "Flavor Profile: " + flavor["FLAVOR_PROFILE"] + "\n" +
        "Form: " + flavor.FORM + "\n" +
        "Descriptors: " + flavor.DESCRIPTORS + "\n" +
        "FONA Product Code: " + flavor["FONA_PRODUCT_CODE"] + "\n" +
        "BestCo Item Code: "+flavor["BESTCO_ITEM"]+ "\n",
        // "SAP Descriptor: " + flavor["SAP DESCRIPTION"] + "\n" +
        // "SAP Number: " + flavor.SAP_num + "\n" +
        // "Shipped To: " + flavor["SHIPPED TO"] + "\n" +
        // "Last Sold: " + flavor["LAST SOLD*"] + "\n",
      isHtml: false
    };
    // Send a text message using default options
    this.emailComposer.open(email);
  }

  favoriteFlavor(flavor){
    this.favorited = JSON.parse(JSON.stringify(flavor))
    this.presentFavoritedToast()
    this.isFavorite = true;
  }

  unfavoriteFlavor(flavor){
    console.log("unfavorited")
    this.favorited = "remove"
    this.presentUnfavoritedToast()
    if(this.isFavorite){
      this.isFavorite = false
      this.flavorFlip = true;
    }

  }

  presentFavoritedToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully added to your favorites',
      duration: 2000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentUnfavoritedToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully removed from your favorites',
      duration: 2000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }


}
