import { Component } from "@angular/core";
import { IonicPage } from "ionic-angular"; // NavParams, NavController
import * as moment from "moment";
//import { BarcodeScanner } from "@ionic-native/barcode-scanner";
import { AlertController } from "ionic-angular";
import { ActionSheetController } from "ionic-angular";
//import { PopoverController } from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the QrStudentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-qr-student",
  templateUrl: "qr-student.html"
})
export class QrStudentPage {
  private amount = null;
  private user = null;
  private date = null;
  //private qrData = null;
  private createdCode = null;
  //private selectedDate = null;
  private period = null;
  private options = [
    "this week",
    "this and next week",
    "this month",
    "package"
  ];
  private costs = [20, 40, 60, 150];
  private cost = null;

  constructor(
    //private navCtrl: NavController,
    //private navParams: NavParams,
    //private barcodescanner: BarcodeScanner,
    private alertCtrl: AlertController,
    private actionSheetCtrl: ActionSheetController,
    //private popoverCtrl: PopoverController,
    private auth: AuthenticationProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad QrStudentPage");
  }

  enterVal() {
    this.date = moment()
      .local()
      .toString();
    this.user = this.auth.currentUser;
    this.presentConfirm();
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: "Confirm purchase",
      message:
        "User: " +
        this.user.displayName +
        "<br> is spending: $" +
        this.amount +
        "<br> on " +
        this.date,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            this.amount = null;
          }
        },
        {
          text: "Buy",
          handler: () => {
            var temp = {
              displayName: this.user.displayName,
              uid: this.user.uid,
              photo: this.user.photoURL,
              email: this.user.email,
              amount: this.amount,
              date: this.date,
              paidUntil: this.period,
              cost: this.cost
            };
            this.createdCode = JSON.stringify(temp);
          }
        }
      ]
    });
    alert.present();
  }

  selectPeriod(ev) {
    if (ev == this.options[0]) {
      alert(moment().endOf("week"));
      this.period = moment().endOf("week");
      this.cost = this.costs[this.options.indexOf(this.options[0])];
      alert(this.cost);
    }
    if (ev == this.options[1]) {
      alert(
        moment()
          .add("days", 7)
          .endOf("week")
      );
      this.period = moment()
        .add("days", 7)
        .endOf("week");
      this.cost = this.costs[this.options.indexOf(this.options[1])];
      alert(this.cost);
    }
    if (ev == this.options[2]) {
      alert(moment().endOf("month"));
      this.period = moment().endOf("month");
      this.cost = this.costs[this.options.indexOf(this.options[2])];
      alert(this.cost);
    }
    if (ev == this.options[3]) {
      alert(
        moment()
          .add("weeks", 7)
          .endOf("week")
      );
      this.period = moment()
        .add("weeks", 7)
        .endOf("week");
      this.cost = this.costs[this.options.indexOf(this.options[3])];
      alert(this.cost);
    }
  }

  done() {
    this.createdCode = null;
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Modify your album",
      buttons: [
        {
          text: "Destructive",
          role: "destructive",
          handler: () => {
            console.log("Destructive clicked");
          }
        },
        {
          text: "Archive",
          handler: () => {
            console.log("Archive clicked");
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    actionSheet.present();
  }
}
