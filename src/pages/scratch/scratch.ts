import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-scratch',
  templateUrl: 'scratch.html'
})
export class ScratchPage {
  private heartArray = []
  //private showHeart = false;


  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }
  getCoordinates(event) {
    this.heartArray.map(heart => { heart.color = "green" })
    let header = document.getElementById("header");
    let header_height = header.offsetHeight;
    let x = (event.clientX - 10) + "px";
    let y = (event.clientY - header_height - 10) + "px";

    // build heart object
    let newHeart = {
      name: "heart",
      top: y,
      left: x,
      color: "red",
      position: "absolute"
    }

    console.log(newHeart)
    this.heartArray.push(newHeart);
    console.log(this.heartArray)

  }
  swipe() {
    alert("boo")
  }

  swipeAll(event: any): any {
    console.log('Swipe All', event);
  }

  swipeLeft(event: any): any {
    console.log('Swipe Left', event);
    alert("swipe Left")
  }

  swipeRight(event: any): any {
    console.log('Swipe Right', event);
    alert("swipe Right")
  }

  swipeUp(event: any): any {
    console.log('Swipe Up', event);
    alert("swipe Up")
  }

  swipeDown(event: any): any {
    console.log('Swipe Down', event);
    alert("swipe Down")
  }

}
