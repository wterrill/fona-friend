import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Content } from 'ionic-angular';
import * as anime from 'animejs';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search2',
  templateUrl: 'search2.html',
})
export class Search2Page {
  @ViewChild(Content) content: Content;
  //private cards = [1, 2, 3];
  private show_menu = true
  private x;
  private y;
  private expression;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
    var path = anime.path('#motionPath path');

var motionPath = anime({
  targets: '#motionPath .el',
  translateX: path('x'),
  translateY: path('y'),
  rotate: path('angle'),
  easing: 'linear',
  duration: 2000,
  loop: true
});
console.log(motionPath)

  }

  toggle_drop_down_menu() {
    let that = this;
    // this.show_menu = !this.show_menu;
    if (!this.show_menu) {
      this.show_menu = true;
      this.content.resize();
      var cssSelector = anime({
        targets: '.slide_out',
        duration: 300,
        translateY: 0,
        easing: "easeInSine",
        elasticity: 0,
        complete: function (anim) {
          console.log(anim.completed);
          console.log(cssSelector)
        }
      });
    } else {
      cssSelector = anime({
        targets: '.slide_out',
        duration: 300,
        translateY: -200,
        easing: "easeInSine",
        elasticity: 0,
        complete: function (anim) {
          console.log(anim.completed);
          that.show_menu = false;
          that.content.resize();
          console.log(cssSelector)
        }
      });
      console.log(cssSelector)
    }
  }


  getCoordinates(event)
{
    // This output's the X coord of the click
    
    this.x = event.clientX;
    this.y = event.clientY;
    //this.expression = {'position': "absolute", 'top':this.y, 'left':this.x}
    var header = document.getElementById("id");
    var heart = document.getElementById("heart")
    var header_height = header.offsetHeight;
    var heart_height = heart.offsetHeight;
    var heart_width = heart.offsetWidth;

    var y_pos = this.y - header_height - heart_height/2
    var x_pos = this.x - heart_width/2;
    console.log(header.offsetHeight); 
    this.expression = {color:"red",position: "absolute", top: y_pos+"px", left: x_pos+"px"}

    let beer = "<i class=\"fa fa-heart\" style=\"position: absolute; top:"+ (this.y - 10) + "px; left: " + (this.x-10) + "px; color: green;\"  ></i>"

    console.log(beer)

    var d1 = document.getElementById('body');
    d1.insertAdjacentHTML('afterbegin', beer);

    // This output's the Y coord of the click
    console.log(event.clientX);
    console.log(event.clientY);
    console.log(event);
    console.log(this.expression)
    console.log(heart_height, heart_width)
}

}
