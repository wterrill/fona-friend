import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular'; //, NavParams


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  gotoVideo(){
    this.navCtrl.push("VideoPage");
  }

  gotoVideo2(){
    this.navCtrl.push("Video2Page");
  }

  gotoVideo3(){
    this.navCtrl.push("Video3Page");
  }

  gotoVideo4(){
    this.navCtrl.push("Video4Page");
  }

  gotologin(){
    this.navCtrl.push("LoginPage");
  }

  gotoSignUp(){
    this.navCtrl.push("SignUpPage");
  }

  gotopicker(){
    this.navCtrl.push("VideoPickerPage");
  }

  gotoRestrictVideo(){
    this.navCtrl.push("RestrictVideoPage");
  }

  gotoRestrictBrowser(){
    this.navCtrl.push("FileBrowserPage");
  }
  
  gotoComponentTest(){
    this.navCtrl.push("ComponentTestPage");
  }

  gotoQR(){
    this.navCtrl.push("QrCodeStuffPage");
  }

  gotoSlide(){
    this.navCtrl.push("SlideshowPage");
  }

  gotoLanding(){
    this.navCtrl.push("LandingPage");
  }

  gotoQRS(){
    this.navCtrl.push("QrStudentPage");
  }

  gotoQRI(){
    this.navCtrl.push("QrInstructorPage");
  }

  gotoProfile(){
    this.navCtrl.push("CreateProfilePage");
  }
  
  gotoAccounts(){
    this.navCtrl.push("AccountsPage");
  }

  gotoLocalVideo(){
    this.navCtrl.push("LocalVideoPage");
  }

  gotoSearch2(){
    this.navCtrl.push("Search2Page")
  }

  gotoScratch(){
    this.navCtrl.push("ScratchPage")
  }

  
}
