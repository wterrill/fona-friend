import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmailSuggestionPage } from './email-suggestion';

@NgModule({
  declarations: [
    EmailSuggestionPage,
  ],
  imports: [
    IonicPageModule.forChild(EmailSuggestionPage)
  ],
})
export class FeedbackPageModule {}
