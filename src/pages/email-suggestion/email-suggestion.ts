import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-EmailSuggestionPage',
  templateUrl: 'email-suggestion.html',
})
export class EmailSuggestionPage {

  constructor(
    //private navCtrl: NavController,
    //private navParams: NavParams,
    private emailComposer: EmailComposer,
    private auth: AuthenticationProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }


  sendIt(){
    let email = {
      to: '2FonaFriend@gmail.com',
      cc: ['william.terrill@gmail.com', 'jellis@fona.com'],
      //cc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf'
      ],
      subject: 'Suggesstion for Fona Friend from user: '+this.auth.currentUser.email,
      body: 'Please put any suggestions below:',
      isHtml: false
    };  
    // Send a text message using default options
    this.emailComposer.open(email);
  }
}



