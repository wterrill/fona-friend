import { Component, ViewChild } from '@angular/core';
import { Content } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';
import * as Fuse from "fuse.js";
import _ from "lodash";
import { ModalController } from 'ionic-angular';
import { LoadingController } from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { AlertController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { Storage } from "@ionic/storage";
import { ToastController } from 'ionic-angular';
import * as anime from 'animejs';
import { ChangeDetectorRef } from '@angular/core';
//import { Select } from 'ionic-angular';
import { NgZone } from '@angular/core';


@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild(Content) content: Content;
  private resultsArray = null;
  private backupArray = null;

  // variables for search
  //private filteredSearchItems = null;
  //private searchItems = null;
  private searching = null;
  private searchInput = null;
  private addedSearches = [];
  private searchingBackup = null;
  private menu = true;
  private inputClicked = 0;
  private admin = false;
  private favorites = null;
  private heartColor = "heartBlack"
  private showFavorites = false
  private lastMenu = null;
  private selectedNAT_ART = "Any"
  private selectedGMO = "Any"
  private zone = null;
  private resetValue = true;

  //hard inputs
  private selectedForm = "Any";
  private selectedProfile = "Any";
  private selected65 = "Any";
  private selectedHalal = "Any";
  private phys_forms = []
  private nat_arts = []
  private profiles = []
  private gmos = []
  private booleans = []
  private debug = false;

  private loading = null;
  private database = "/flavors/"
  private savedFilters = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    private auth: AuthenticationProvider,
    private alertCtrl: AlertController,
    private emailComposer: EmailComposer,
    private storage: Storage,
    private toastCtrl: ToastController,
    public cdr: ChangeDetectorRef
  ) {
    this.phys_forms = ["Any", "Powder-WS", "Liquid-OS", "Liquid-WS"]
    this.nat_arts = ["Any", " Nat", "Art ", "Nat & Art", "Nat WONF"]
    this.profiles = ["Any", "Berry", "Brown", "Citrus", "Fantasy", "Grape", "Herbal", "Melon", "Mint", "Modifier", "Orchard", "Spice", "Stone Fruit", "Tropical"]
    this.gmos = ["Any", "GM ", "GMO-Free", "Non-GMO"]
    this.booleans = ["Any", "Yes", "No"]

    this.storage.get("savedFilters").then(val => {
      if (val) {
        this.savedFilters = JSON.parse(JSON.stringify(val))
        //this.savedFilters = val 
      } else {
        this.savedFilters = [];
      }
    });

    this.storage.get("favorites").then(val => {
      if (val) {
        this.favorites = JSON.parse(JSON.stringify(val))
        //console.log("Hello")
        //this.savedFilters = val 
      } else {
        this.favorites = [];
      }
      this.prepData()
    });
    this.zone = new NgZone({ enableLongStackTrace: false });


  }
  updateFavorites(favArray, targetArray) {
    var index = targetArray.map(res => {
      res.isFavorite = false;
      favArray.forEach(fav => {

        if (res.ID === fav.ID) {
          res.isFavorite = true;
        }
      })
    })
    console.log(index)
    //console.log("after favorite update:", targetArray)
    return targetArray
  }

  ionViewDidLoad() {

  }

  prepData() {
    //console.log('ionViewDidLoad FonaTestPage');
    this.loading = this.loadingCtrl.create({
      spinner: "ios",
      content: "Loading Flavors..."
    });
    this.loading.present();
    if (this.auth.currentUserData.permissions === "administrator") {
      this.admin = true;
      this.database = "/BESTCO/"
    }
    this.getData();
    this.loading.dismiss();
  }

  getData() {
    firebase
      .database()
      .ref(this.database)
      .once("value")
      .then(snapshot => {
        //.orderByChild('displayName');
        this.resultsArray = snapshot.val();
        this.resultsArray = this.updateFavorites(this.favorites, this.resultsArray);
        this.backupArray = this.resultsArray;
        //this.addPlaceholder();
        //console.log(this.resultsArray)
      }); //end firebase.database
  }

  selectForm(event) {
    //this.selectedForm = event;
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["FORM"])
      this.addedSearches.push("form:" + event)
    }
    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        //let tag = temp[1];
        if (type === "form") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("form:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("form:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    }
    this.inputClicked = 0;
    // this.content.resize();
    // this.cdr.detectChanges();
    this.hackyCode()
  }

  selectNAT_ART(event) {
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["NAT_ART"])
      this.addedSearches.push("nat-art:" + event)
    }
    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        //let tag = temp[1];
        if (type === "nat-art") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("nat-art:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("nat-art:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    }
    this.inputClicked = 0;
    //this.cdr.detectChanges();
    this.hackyCode();
  }

  selectProfile(event) {
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["FLAVOR_PROFILE"])
      this.addedSearches.push("prof:" + event)
      //this.addPlaceholder();
    }

    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        //let tag = temp[1];
        if (type === "prof") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("prof:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("prof:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);

      }
    }
    this.inputClicked = 0;
    // this.content.resize();
    // this.cdr.detectChanges();
    this.hackyCode()
  }

  selectGMO(event) {
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["GMO_STATUS"])
      this.addedSearches.push("gmo:" + event)
      //this.addPlaceholder();
    }

    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        //let tag = temp[1];
        if (type === "gmo") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("gmo:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("gmo:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    }
    this.inputClicked = 0;
    //this.content.resize();
    //this.cdr.detectChanges();
    this.hackyCode()
  }

  select65(event) {
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["PROP65"])
      this.addedSearches.push("65:" + event)
    }
    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        //let tag = temp[1];
        if (type === "65") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("65:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("65:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    }
    this.inputClicked = 0;
    // this.content.resize();
    // this.cdr.detectChanges();
    this.hackyCode()
  }

  selectHalal(event) {
    //case 1: form selected is specific, and no other searches are in the search tree
    if (event !== "Any" && this.addedSearches.length === 0) {
      this.resultsArray = this.fuseSearch(event, this.resultsArray, ["HALAL"])
      this.addedSearches.push("halal:" + event)
    }
    //case 2: form selected is specific, and no other form was previously selected
    else if (this.addedSearches.length > 0) {
      let formFound = true;
      //search for form in previous searched
      for (let i = 0; i < this.addedSearches.length; i++) {
        let temp = this.addedSearches[i].split(":");
        let type = temp[0];
        //let tag = temp[1];
        if (type === "halal") {
          this.eraseTag(this.addedSearches[i], false);
          //case 3: selected open searches
          if (event !== "Any") {
            this.addedSearches.push("halal:" + event);
          }
          this.arraySearch(this.addedSearches, this.backupArray);

          formFound = false
        }
      }
      if (formFound && (event !== "Any")) {
        this.addedSearches.push("halal:" + event)
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    }
    this.inputClicked = 0;
    // this.content.resize();
    // this.cdr.detectChanges();
    this.hackyCode()
  }

  hackyCode() {
    this.resetValue = false;
    //this.menu = !this.menu;
    let hideFooterTimeout = setTimeout(() => {
      this.resetValue = true;
      //this.menu = !this.menu;
      this.content.resize();
    }, 1);
    console.log(hideFooterTimeout)
    //this.content.resize();

  }

  getItems(ev: any) {
    // Reset items back to all of the items
    //this.initializeItems();
    if (this.debug) {
      //console.log("searchItems", this.searchItems);
      //console.log("this.resultsArray", this.resultsArray);
    }

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (this.debug) {
      //console.log("ev", ev);
      //console.log("val", val);
      //console.log(ev.data);
    }
    if (ev.data == null) {
      // if this is null, then the user hit 'backspace' to kill everything
      //this.updatelist;   // so reset the filters to show the restaurants again.
    }

    if (val == null) {
      //this.reset();
    }

    if (val != null && val.length < 1 && this.searching == true) {
      //this case is invoked if we were searching, but we deleted to start again.
      this.searching = false; //this.searching tells the html that we are actively searching for stuff, so it changes what is displayed.
      //this.getData();
    }

    if (val != null && val.length == 0) {
      this.resultsArray = this.searchingBackup;
    }
    if (val == null) {
      this.resultsArray = this.searchingBackup;
    }

    if (val != null && val.length > 0) {
      this.resultsArray = this.fuseSearch(val, this.searchingBackup, ["FONA_PRODUCT_CODE"]); //Search it!!!
    }
    this.content.resize();
  }

  saveInputResults() {
    if (this.inputClicked < 1) {
      this.searchingBackup = this.resultsArray;
      this.inputClicked++;
    }
  }

  reset() {
    this.resultsArray = this.backupArray;
    this.addedSearches = [];
    this.searchInput = null;
    this.selectedForm = "Any";
    this.selectedProfile = "Any";
    this.selected65 = "Any";
    this.selectedGMO = "Any";
    this.selectedHalal = "Any";
    this.selectedNAT_ART = "Any";
    this.inputClicked = 0;
    this.presentResetToast();
    this.content.resize();
    //this.cdr.detectChanges();
    console.log("*******************************")
    console.log(this.selectedNAT_ART)
    console.log(this.selectedHalal)
    //this.resetValue = false;
    //this.resetValue = true;
  }

  save() {
    if (this.addedSearches.length != 0) {
      this.presentSaveName()
    }
    else {
      this.presentSavedError()
    }
  }

  saveFilters(name, filters) {
    this.savedFilters.push([name, filters])
    this.storage.set("savedFilters", this.savedFilters)
    this.presentFilterToast()
  }

  show() {
    this.presentSavedModal(this.savedFilters);
  }

  book() {
    this.presentSearchModal(this.resultsArray)
  }

  presentSavedModal(savedFilters) {
    const contactModal1 = this.modalCtrl.create("SavedFiltersPage", { savedFilters: savedFilters });
    contactModal1.onDidDismiss(data => {
      if (data) {
        this.addedSearches = JSON.parse(JSON.stringify(data));
        this.arraySearch(this.addedSearches, this.backupArray);
      }
    });
    contactModal1.present();
  }

  presentSearchModal(resultsArray) {
    const contactModal2 = this.modalCtrl.create("DescriptorSearchPage", { resultsArray: resultsArray });
    contactModal2.onDidDismiss(data => {
      if (data.length > 0) {
        data.map(x => {
          this.addedSearches.push("desc:" + x)
        });
        //this.addedSearches = JSON.parse(JSON.stringify(data));
        this.arraySearch(this.addedSearches, this.resultsArray);
        this.content.resize();
      }
    });
    contactModal2.present();
  }

  noResultsEmail() {
    this.sendNoResults(this.addedSearches, this.searchInput)
  }
  sendNoResults(addedSearches, inputVal) {
    let searchBarText = ""
    if (inputVal != null) {
      searchBarText = "Descriptor Search Bar Input:\n" + inputVal + "\n"
    }
    let addedSearchText = "";
    if (addedSearches.length > 0) {
      addedSearchText = "Added Search Filters:\n" + addedSearches.toString().replace(/,/g, "\n").replace(/form:/g, "Form: ").replace(/prof:/g, "Flavor Profile: ").replace(/desc:/g, "Descriptor: : ") + "\n"
    }
    let email = {
      to: '2FonaFriend@gmail.com',
      cc: ['william.terrill@gmail.com', 'jellis@fona.com'],
      //cc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf'
      ],
      subject: 'NO RESULTS Search from user: ' + this.auth.currentUser.email,
      body: "The following search yielded in NO RESULT: \n" +
        addedSearchText +
        searchBarText +
        "Please enter any comments below:",
      isHtml: false
    };
    // Send a text message using default options
    this.emailComposer.open(email);
  }


  fuseSearch(searchTerm, arrayToBeSearched, keys) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: keys  ///////////////////////////////
    };

    if (this.debug) {
      //console.log("fuzzywuzzy", searchTerm);
    }
    var fuse = new Fuse(arrayToBeSearched, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchTerm);
    if (this.debug) {
      //console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      el.item.matches = el.matches; // the matches matrix shows what actually matched during the search.  We're storing it inside of filteredSearchItems
      var matchedFoodArray = _.map(el.matches, function (le) {
        if (le.key == "menu_items.name") {
          return el.item.menu_items[le.arrayIndex];
        } //Here's we're using the array index for the matching items to look up the actual menu items.
      });
      if (matchedFoodArray[0] != undefined) {
        el.item.matching_food = matchedFoodArray; //the matching_food array is the one that will be displayed on the search screen results.
      }
      return el.item;
    });

    if (this.debug) {
      //console.log("final array", arr);
    }
    arr = _.sortBy(arr, 'ID'); // This sorts the array by ID
    return arr;
  } // end of fuse search

  showInfo($event, flavor, index) {
    this.presentFlavorModal(flavor, index, this.resultsArray,false);
    event.stopPropagation();
  }

  showInfoFavorite($event, flavor, index) {
    this.presentFlavorModal(flavor, index, this.favorites,true);
    event.stopPropagation();
  }


  press($event, flavor) {
    this.presentLongPressMenu(flavor)
  }

  pressFavorites($event, flavor, i) {
    this.presentRemoveFavorite(flavor, i)
  }

  swipeToDeleteFavorite($event, flavor, i) {
    var cssSelector = anime({
      targets: ".beer"+flavor.ID,
      duration: 300,
      translateX: -1000,
      easing: "easeOutSine",
      elasticity: 0,
      complete: function (anim) {
        // alert("done")
      }
    });
    this.presentRemoveFavorite(flavor, i)
    console.log(cssSelector)
  }


  presentFlavorModal(flavor, index, array, fromFavorite) {
    //check to see if flavor is already a favorite
    //console.log(flavor)
    //console.log(this.favorites)
    var obj = this.favorites.find((obj) => {
      //console.log("here")
      return obj.id === flavor.id;
    });
    //console.log(obj)
    let isFavorite = false
    if (obj) {
      isFavorite = true
    }

    // const contactModal3 = this.modalCtrl.create("FlavorInfoPage", { flavorID: flavor, isFavorite: isFavorite });
    const contactModal3 = this.modalCtrl.create("FlavorInfoSlidePage", { flavor: flavor, resultsArray: array, index: index, favorites: this.favorites, fromFavorite: fromFavorite});
    contactModal3.onDidDismiss(data => {
      this.favorites = data;
      this.updateFavorites(this.favorites, this.resultsArray);
      this.updateFavorites(this.favorites, this.backupArray);
      this.storage.set("favorites", this.favorites)
      // if (data) {
      //   console.log(data)
      //   if (data === "remove") {
      //     console.log(this.favorites)
      //     this.favorites = this.favorites.filter((obj)=> { return obj.id !== flavor.id; }) //removed previously selected flavor from favorites
      //     console.log(this.favorites)
      //   } else {
      //   this.favorites.push(data) //JSON.parse(JSON.stringify(data));
      //   this.storage.set("favorites", this.favorites)
      //   };
      // }
    });
    contactModal3.present();
  }

  addSearch() {
    this.addedSearches.push("desc:" + this.searchInput)
    this.searchInput = null;
    this.inputClicked = 0;
    this.content.resize();
  }

  eraseTag(tag, fromTag) {
    this.resetValue = false;
    let temp = tag.split(":");
    let type = temp[0]
    //let tagname = temp[1]
    //this.zone.run(()=>{ //change detection when eraseing
    if (fromTag) {
      if (type === "form") {
        this.selectedForm = "Any";
        //this.selectForm("Any");
      }

      if (type === "prof") {
        this.selectedProfile = "Any";
        //this.selectProfile("Any");
      }

      if (type === "nat-art") {
        this.selectedNAT_ART = "Any";
        //this.selectNAT_ART("Any");
      }

      if (type === "gmo") {
        this.selectedGMO = "Any";
        //this.selectGMO("Any")
      }

      if (type === "65") {
        this.selected65 = "Any";
        //this.select65("Any")
      }
      if (type === "halal") {
        this.selectedHalal = "Any";
        //this.selectHalal("Any");
      }
    }
    //});
    var index = this.addedSearches.indexOf(tag);
    if (index > -1) {
      this.addedSearches.splice(index, 1);
    }
    if (this.addedSearches.length > 0) {
      this.arraySearch(this.addedSearches, this.backupArray);
    } else {
      this.resultsArray = this.backupArray;
    }
    //this.resetValue = true;
    //this.cdr.detectChanges();
    this.hackyCode();
  }

  arraySearch(searchArray, arrayToBeSearched) {
    if (searchArray.length == 0) {
      this.reset();
    } else {
      var tempArray = []
      for (let i = 0; i <= searchArray.length - 1; i++) {
        //first let's split up the tag so that we can do the right search
        let tag = searchArray[i].split(":");
        let type = tag[0];
        let value = tag[1];

        if (type == "desc") {
          // console.log("desc");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["id", "DESCRIPTORS"])
          //console.log(tempArray);
        }

        if (type == "form" && value != "Any") {
          // console.log("form");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["FORM"])
          //console.log(tempArray);
        }

        if (type == "prof" && value != "Any") {
          // console.log("prof");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["FLAVOR_PROFILE"])
          //console.log(tempArray);
        }

        if (type == "nat-art" && value != "Any") {
          // console.log("nat-art");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["NAT_ART"])
          //console.log(tempArray);
        }

        if (type == "gmo" && value != "Any") {
          // console.log("gmo");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["GMO_STATUS"])
          console.log(tempArray);
        }

        if (type == "65" && value != "Any") {
          // console.log("65");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["PROP65"])
          //console.log(tempArray);
        }

        if (type == "halal" && value != "Any") {
          // console.log("65");
          // console.log(searchArray)
          // console.log(arrayToBeSearched);
          // console.log(tempArray);

          tempArray = this.fuseSearch(value, arrayToBeSearched, ["HALAL"])
          // console.log(tempArray);
        }


        arrayToBeSearched = tempArray;
      }
      this.resultsArray = tempArray;
    }
  }

  // toggleMenu() {
  //   if (!this.menu) {
  //     this.menu = true;
  //     this.content.resize();
  //     var cssSelector = anime({
  //       targets: '.slide_out',
  //       duration: 300,
  //       translateY: 0,
  //       easing: "easeInSine",
  //       elasticity: 0,
  //       complete: function (anim) {
  //         //console.log(anim.completed);
  //       }
  //     });
  //   } else {
  //     var cssSelector = anime({
  //       targets: '.slide_out',
  //       duration: 300,
  //       translateY: -220,
  //       easing: "easeInSine",
  //       elasticity: 0,
  //       complete: (anim) => {
  //         //console.log(anim.completed);
  //         this.menu = false;
  //         this.content.resize();
  //       }
  //     });
  //   }
  // }

  panEvent(e){
    console.log(e)
  }

  menuOpen(){
      this.menu = true;
      this.content.resize();
      var cssSelector = anime({
        targets: '.slide_out',
        duration: 300,
        translateY: 0,
        easing: "easeInSine",
        elasticity: 0,
        complete: function (anim) {
          //console.log(anim.completed);
        }
      });
      console.log(cssSelector)
  }

  menuClose(){
      var cssSelector = anime({
        targets: '.slide_out',
        duration: 300,
        translateY: -220,
        easing: "easeInSine",
        elasticity: 0,
        complete: (anim) => {
          //console.log(anim.completed);
          this.menu = false;
          this.content.resize();
        }
      });
      console.log(cssSelector)
  }

  toggleFavorites() {
    if (this.showFavorites) {
      this.menu = this.lastMenu;
    } else {
      this.lastMenu = this.menu;
      this.menu = false;
      //this.menu_show = false
    }

    this.showFavorites = !this.showFavorites;
    if (this.showFavorites) {
      this.heartColor = "heartRed"
    } else {
      this.heartColor = "heartBlack"
      //this.menu_show = true;
    }
    this.content.resize();

  }

  presentLongPressMenu(flavor) {
    let alert = this.alertCtrl.create({
      title: "Flavor Options?",
      message: "Would you like to email FONA International about this flavor, or add it to your favorites?",
      buttons: [
        {
          text: "Add to Favorites",
          role: "cancel",
          handler: () => {
            this.addToFavorites(flavor)
          }
        },
        {
          text: "Email!",
          role: "cancel",
          handler: () => {
            this.sendItFlavor(flavor)
          }
        }, {
          text: "Cancel",
          role: "cancel",
          handler: () => {
          }
        }
      ],
      cssClass: "alertcss"
    });
    alert.present();
  }

  addToFavorites(flavor){
    this.favorites.push(flavor)
    this.storage.set("favorites", this.favorites)
    this.presentFavoriteToast()
    this.updateFavorites(this.favorites, this.resultsArray)
    this.updateFavorites(this.favorites, this.backupArray)
  }

//   findWithAttr(array, attr, value) {
//     for(var i = 0; i < array.length; i += 1) {
//         if(array[i][attr] === value) {
//             return i;
//         }
//     }
//     return -1;
// }

  removeFromFavorites(flavor){
    //var index = this.findWithAttr(this.favorites,'ID',flavor.ID)
    var index = this.favorites.map(function(e) { return e.name; }).indexOf(flavor.ID);
    this.favorites.splice(index, 1);
    this.storage.set("favorites", this.favorites);
    this.presentUnFavoriteToast();
    this.updateFavorites(this.favorites, this.resultsArray)
    this.updateFavorites(this.favorites, this.backupArray)
  }

  presentSaveName() {
    let alert = this.alertCtrl.create({
      title: "Name Search Filters",
      message: "Enter a name for the filters you'd like to save.",
      inputs: [
        {
          name: 'savedName',
          placeholder: 'Saved Filter Name'
        },
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            //console.log("cancelled");
          }
        },
        {
          text: "OK!",
          //role: "cancel",
          handler: data => {
            if (data.savedName) {
              this.saveFilters(data.savedName, this.addedSearches)
            }
          }
        }
      ],
      cssClass: "alertcss"
    });
    alert.present();
  }

  presentRemoveFavorite(flavor, i) {
    let alert = this.alertCtrl.create({
      title: "Remove From Favorites",
      message: "Would you like to remove " + this.favorites[i]["FLAVOR_NAME"] + " from your favorites?",
      // inputs: [
      //   {
      //     name: 'savedName',
      //     placeholder: 'Saved Filter Name'
      //   },
      // ],
      buttons: [
        {
          text: "No",
          role: "cancel",
          handler: () => {
            var cssSelector = anime({
              targets: ".beer"+flavor.ID,
              duration: 1000,
              translateX: 0,
              easing: "easeOutElastic",
              elasticity: 0,
              complete: function (anim) {
                console.log(cssSelector)
                // alert("done")
              }
            });
          }
        },
        {
          text: "Yes",
          //role: "cancel",
          handler: data => {
            this.favorites.splice(i, 1);
            this.updateFavorites(this.favorites, this.resultsArray);
            this.updateFavorites(this.favorites, this.backupArray);
            this.storage.set("favorites", this.favorites);
          }
        }
      ],
      cssClass: "alertcss"
    });
    alert.present();
  }

  sendItFlavor(flavor) {
    let email = {
      to: '2FonaFriend@gmail.com',
      cc: ['william.terrill@gmail.com', 'jellis@fona.com'],
      //cc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf'
      ],
      subject: 'Flavor Info: ' + flavor["PRODUCT_NAME"] + ' from user: ' + this.auth.currentUser.email,
      body: "The flavor you\'d like information is shown below.  What question do you have?: \n\n\n\n\n\n\n" +
        "Name: " + flavor["PRODUCT_NAME"] + "\n" +
        "Flavor Profile: " + flavor["FLAVOR_PROFILE"] + "\n" +
        "Form: " + flavor.FORM + "\n" +
        "Descriptors: " + flavor.DESCRIPTORS + "\n" +
        "FONA Product Code: " + flavor["FONA_PRODUCT_CODE"] + "\n",
      // "SAP Descriptor: " + flavor["SAP DESCRIPTION"] + "\n" +
      // "SAP Number: " + flavor.SAP_num + "\n" +
      // "Shipped To: " + flavor["SHIPPED TO"] + "\n" +
      // "Last Sold: " + flavor["LAST SOLD*"] + "\n",
      isHtml: false
    };
    // Send a text message using default options
    this.emailComposer.open(email);
  }

  sendItSearch() {
    let email = {
      to: '2FonaFriend@gmail.com',
      cc: ['william.terrill@gmail.com', 'jellis@fona.com'],
      //cc: ['john@doe.com', 'jane@doe.com'],
      attachments: [
        // 'file://img/logo.png',
        // 'res://icon.png',
        // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
        // 'file://README.pdf'
      ],
      subject: 'Search Help Info from user: ' + this.auth.currentUser.email,
      body: "It seems that the following search you made didn't have any results:\n" +
        "Previous search filters: " + this.addedSearches + "\n" +
        "Currently searching descriptor: " + this.searchInput + "\n" +
        + "What help would you like from FONA?",
      isHtml: false
    };
    // Send a text message using default options
    this.emailComposer.open(email);
  }



  presentSavedError() {
    let alert = this.alertCtrl.create({
      title: "Save Search Error",
      message: "You will need to select some filters such as Form, Flavor Profile or Descriptors in order to use this feature",
      // inputs: [
      //   {
      //     name: 'savedName',
      //     placeholder: 'Saved Filter Name'
      //   },
      // ],
      buttons: [
        {
          text: "OK",
          role: "cancel",
          // handler: data => {
          //   if(data.savedName){
          //     this.saveFilters(data.savedName, this.addedSearches)
          //   }
          // }
        }
      ],
      cssClass: "alertcss"
    });
    alert.present();
  }

  presentFilterToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully saved filter',
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
  }

  presentResetToast() {
    let toast = this.toastCtrl.create({
      message: 'Your filters have been reset',
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
  }

  presentFavoriteToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully added to your favorites',
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
  }

  presentUnFavoriteToast() {
    let toast = this.toastCtrl.create({
      message: 'Successfully removed from your favorites',
      duration: 2000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
  }

}
