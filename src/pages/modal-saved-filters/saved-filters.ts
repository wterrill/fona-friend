import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Storage } from "@ionic/storage";


/**
 * Generated class for the FlavorInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-saved-filters',
  templateUrl: 'saved-filters.html',
})
export class SavedFiltersPage {
  private savedFilters = null;
  //private highlight = null

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private alertCtrl: AlertController,
    private storage: Storage
  ) {
    this.savedFilters = navParams.get('savedFilters')
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
  }

  selectSavedFilter(filters) {
    let data = filters;
    this.viewCtrl.dismiss(data);
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

  deleteSavedFilter(name,index){
    this.presentConfirmDelete(name,index)
  }

  presentConfirmDelete(name,index) {
    let alert = this.alertCtrl.create({
      title: "Delete Saved Search?",
      message:
        "Would you like to delete your saved search: " +name,
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          // handler: () => {
          //   this.amount = null;
          // }
        },
        {
          text: "Yes",
          handler: () => {
            if (index > -1) {
              this.savedFilters.splice(index, 1);
              this.storage.set("savedFilters",this.savedFilters)
            }
          }
        }
      ],
      cssClass: "alertcss"
    });
    alert.present();
  }

}
