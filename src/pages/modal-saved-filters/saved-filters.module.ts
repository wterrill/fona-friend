import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedFiltersPage } from './saved-filters';

@NgModule({
  declarations: [
    SavedFiltersPage,
  ],
  imports: [
    IonicPageModule.forChild(SavedFiltersPage),
  ],
})
export class FlavorInfoPageModule {}
