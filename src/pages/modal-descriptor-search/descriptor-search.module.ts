import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptorSearchPage } from './descriptor-search';

@NgModule({
  declarations: [
    DescriptorSearchPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptorSearchPage),
  ],
})
export class FlavorInfoPageModule {}
