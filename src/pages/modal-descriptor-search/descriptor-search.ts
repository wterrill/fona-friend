import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
//import { AlertController } from 'ionic-angular';
//import { Storage } from "@ionic/storage";
//import * as anime from 'animejs';
import { Content } from 'ionic-angular';
import * as Fuse from "fuse.js";
import _ from "lodash";
import { Searchbar } from 'ionic-angular';


/**
 * Generated class for the FlavorInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-descriptor-search',
  templateUrl: 'descriptor-search.html',
})
export class DescriptorSearchPage {
  @ViewChild(Content) content: Content;
  @ViewChild('mySearchbar') searchbar: Searchbar;
  private resultsArray = null;
  private descriptorArray = [];
  private cssArray = [];
  private btn_unselect_class = "btn_unselect";
  private btn_select_class = "btn_select";
  //private admin = true;
  //private body = true;
  private selectedDescriptors = []
  private searching = null;
  private debug = true;
  private searchingBackup = [];
  private backupDescriptorArray = [];
  private backupResultsArray = [];
  private searchInput = ""

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    //private alertCtrl: AlertController,
    //private storage: Storage
  ) {
    this.resultsArray = navParams.get('resultsArray')
    console.log("resultsArray",this.resultsArray)
    this.backupResultsArray = this.resultsArray;
    this.descriptorArray = this.buildDescriptorArray(this.resultsArray)
    this.backupDescriptorArray = this.descriptorArray
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserInfoPage');
  }

  dismiss() {
    //let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(this.selectedDescriptors);
  }

  buildDescriptorArray(fromArray) {
    let toArray = [];
    fromArray.map(x => {
      var stringArray = x["DESCRIPTORS"].split(', ')
      console.log("stringArray:",stringArray)

      stringArray.map(y => {
        console.log("y:",y)
        y = y.replace(/^\s+|\s+$/g, '');
        console.log("y:",y)
        if (toArray.indexOf(y) < 0) {
          if(y==""){
            console.log("found it")
          }
          toArray.push(y)
          this.cssArray.push(this.btn_unselect_class)
        }
      })
    });
    console.log("toArray presort:",toArray)
    toArray.sort();
    console.log("toArray postsort:",toArray)
    return toArray;
  }

  addSelection(descriptor) {
    let cssIndex = this.backupDescriptorArray.indexOf(descriptor)
    if (this.cssArray[cssIndex] == this.btn_select_class) {
      this.searchInput = ""
      this.cssArray[cssIndex] = this.btn_unselect_class
      let spliceIndex = this.selectedDescriptors.indexOf(descriptor)
      this.selectedDescriptors.splice(spliceIndex, 1);
      let temp = this.arraySearch(this.selectedDescriptors, this.backupResultsArray);
      if (temp) {
        this.descriptorArray = this.buildDescriptorArray(temp)
      }
    } else {
      this.searchInput = ""
      this.cssArray[cssIndex] = "btn_select"
      this.selectedDescriptors.push(descriptor)
      //let temp = this.fuseSearch(descriptor, this.resultsArray, ["DESCRIPTORS"])
      let temp = this.arraySearch(this.selectedDescriptors, this.resultsArray);
      this.descriptorArray = this.buildDescriptorArray(temp)
    }
    this.content.resize(); //content needs to be resizeds since we changed the filter tags as well.
  }

  arraySearch(searchArray, arrayToBeSearched) {
    if (searchArray.length == 0) {
      this.reset();
    } else {
      var tempArray = []
      for (let i = 0; i <= searchArray.length - 1; i++) {
        //first let's split up the tag so that we can do the right search
        // let tag = searchArray[i].split(":");
        // let type = tag[0];
        let value = searchArray[i];

        console.log("desc");
        console.log(searchArray)
        console.log(arrayToBeSearched);
        console.log(tempArray);

        tempArray = this.fuseSearch(value, arrayToBeSearched, ["DESCRIPTORS"])
        console.log(tempArray);

        arrayToBeSearched = tempArray;
      }
      this.resultsArray = tempArray;
      return tempArray;
    }
  }


  saveInputResults() {
    //if (this.inputClicked < 1) {
    this.searchingBackup = this.descriptorArray;
    //   this.inputClicked++;
    // }
  }
  getCssClass(descriptor, i) {
    let cssIndex = this.backupDescriptorArray.indexOf(descriptor)
    return this.cssArray[cssIndex]
  }

  getItems(ev: any) {
    if (this.debug) {
      console.log("this.descriptorArray", this.descriptorArray);
    }
    // set val to the value of the searchbar
    let val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (this.debug) {
      console.log("ev", ev);
      console.log("val", val);
      console.log(ev.data);
    }
    if (ev.data == null) {
      //if this is null, then the user hit 'backspace' to kill everything
      this.reset();   // so reset the filters to show the previous list again.
      this.descriptorArray = this.fuseSearch2(val, this.descriptorArray, "");
      this.descriptorArray.sort();
    }

    if (val == null) {
      this.reset();
    }

    if (val != null && val.length < 1 && this.searching == true) {
      //this case is invoked if we were searching, but we deleted to start again.
      this.searching = false; //this.searching tells the html that we are actively searching for stuff, so it changes what is displayed.
      //this.getData();
    }

    if (val != null && val.length == 0) {
      this.descriptorArray = this.searchingBackup;
    }
    if (val == null) {
      this.descriptorArray = this.searchingBackup;
    }

    if (val != null && val.length > 0) {
      // this.resultsArray = this.fuseSearch(val, this.searchingBackup, ["id", "DESCRIPTORS"]); //Search it!!!
      this.descriptorArray = this.fuseSearch2(val, this.descriptorArray, ""); //Search it!!!
      this.descriptorArray.sort();
    }
  }



  reset() {
    this.descriptorArray = this.backupDescriptorArray;
    this.resultsArray = this.backupResultsArray;
  }

  fuseSearch(searchTerm, arrayToBeSearched, keys) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: keys  ///////////////////////////////
    };

    if (this.debug) {
      console.log("fuzzywuzzy", searchTerm);
    }
    var fuse = new Fuse(arrayToBeSearched, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchTerm);
    if (this.debug) {
      console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      console.log("el:" + JSON.stringify(el))
      return el.item
      // if(el.matches.length!=0){
      // return el.matches[0].value;
      // } else {
      //   return ""
      // }
    });

    if (this.debug) {
      console.log("final array", arr);
    }
    return arr;
  } // end of fuse search  

  fuseSearch2(searchTerm, arrayToBeSearched, keys) {
        //this does the actual fuzzy search.
        var options = {
          shouldSort: true,
          matchAllTokens: true,
          tokenize: true,
          includeScore: true,
          includeMatches: true,
          threshold: 0.1,
          //location: 0,  these values aren't needed if you match all tokens.
          //distance: 100,
          maxPatternLength: 32,
          minMatchCharLength: 1,
          findAllMatches: true,
          keys: keys  ///////////////////////////////
        };
    
        if (this.debug) {
          console.log("fuzzywuzzy", searchTerm);
        }
        var fuse = new Fuse(arrayToBeSearched, options); //this.result is the full-on data.
        var searchResults = fuse.search(searchTerm);
        if (this.debug) {
          console.log("fuzzywuzzy-results", searchResults);
        }
    
        //Massaging the data before returning it. I'm taking the meta-data provided by
        //Fuse, and sticking it back into the filtered results.
        //the lodash map function below steps through each search result...
        var arr = _.map(searchResults, function (el) {
          console.log("el:"+JSON.stringify(el))
          if(el.matches.length!=0){
          return el.matches[0].value;
          } else {
            return ""
          }
        });
    
        if (this.debug) {
          console.log("final array", arr);
        }
        return arr;
    } // end of fuse search

    eraseTag(tag, fromTag) {
      
      this.content.resize();
    }
}
