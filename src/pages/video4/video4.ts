import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
// import { TablePage } from "../table/table";
import { File } from "@ionic-native/file";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer"; //FileUploadOptions
import { ToastController } from 'ionic-angular';


/**
 * Generated class for the Video4Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-video4",
  templateUrl: "video4.html"
})
export class Video4Page {
  ////////////////////////////////////////////
  ///// G L O B A L    V A R I A B L E S//////
  ////////////////////////////////////////////
  // public routine1_OLD = [
  //   ["1", "1 2 3", "Approach for cross body (to the side)"],
  //   ["", "5 6 7", "Cross body turn"],
  //   ["2", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body, Go to open hands"],
  //   ["3", "1 2 3", "Step to the side, grab her right hand with your left"],
  //   [
  //     "",
  //     "5 6 7",
  //     "She takes one step forward and then you redirect her to do a turn."
  //   ],
  //   ["4", "1 2 3", "Over the top catch"],
  //   ["", "5 6 7", "Cross body rolling"],
  //   ["5", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body, end by prepping for across and turn out"],
  //   [
  //     "6",
  //     "1 2 3",
  //     "(across) Both step to your right, my left holds her right hand"
  //   ],
  //   ["", "5 6 7", "Push her hand so that both turn out"],
  //   ["7", "1 2 3", "Approach for cross body turn"],
  //   ["", "5 6 7", "Finish cross body turn"],
  //   [
  //     "8",
  //     "1 2 3",
  //     "Back step (take it back) and approach to the side to set up turn turn turn"
  //   ],
  //   ["", "5 6 7", "Turn turn turn"],
  //   ["9", "1 2 3", "Turn turn turn"],
  //   ["", "5 6 7", "Turn turn turn"],
  //   ["10", "1 2 3", "Approach for  cross body turn"],
  //   ["", "5 6 7", "Cross body turn"],
  //   ["11", "1 2 3", "Approach for cross body turn"],
  //   ["", "5 6 7", "Cross body turn"]
  // ];

  public routine1 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body turn"],
    ["2", "1 2 3", "Basic","7"],
    ["", "5 6 7", "Cross body to open basic"],
    ["3", "1 2 3", "Hands together switch","10"],
    ["", "5 6 7", "Cross body turn"],
    ["4", "1 2 3", "Basic over the top","14"],
    ["", "5 6 7", "Cross body rolling"],
    ["5", "1 2 3", "Basic","18"],
    ["", "5 6 7", "Cross body lead out"],
    ["6", "1 2 3", "Across","22"],
    ["", "5 6 7", "To the turnout on (five six seven)"],
    ["7", "1 2 3", "Basic","26"],
    ["", "5 6 7", "Cross body swing out"],
    ["8", "1 2 3", "take the step back on the back leg, go around.","30"],
    ["", "5 6 7", "(Turn turn turn)"],
    ["9", "1 2 3", "(Turn turn turn)","33.5"],
    ["", "5 6 7", "(Turn turn turn)"],
    ["10", "1 2 3", "Basic","37"],
    ["", "5 6 7", "Cross body (not shown)"],
    ["11", "1 2 3", "Basic (not shown)"],
    ["", "5 6 7", "Cross body (not shown)"]
  ];

  // private routine2_OLD = [
  //   ["1", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body"],
  //   ["2", "1 2 3", "Go to open hand basic"],
  //   [
  //     "",
  //     "5 6 7",
  //     "Step to side, switch hands, and grab other hand underneath. Two hand cross body turn"
  //   ],
  //   ["3", "1 2 3", "Over the top"],
  //   ["", "5 6 7", "Rolling turn"],
  //   ["4", "1 2 3", "catch , step to the side for cross body"],
  //   ["", "5 6 7", "Cross body"],
  //   ["5", "1 2 3", "Approach for cross body in closed position"],
  //   ["", "5 6 7", "Cross body and set up for switch across"],
  //   ["6", "1 2 3", "Grab hand underneath and two hand turnout"],
  //   ["", "5 6 7", "Finish two hand turnout"],
  //   ["7", "1 2 3", "step back with hands joined and crossed"],
  //   [
  //     "",
  //     "5 6 7",
  //     "Step towards her while moving right hands over leads head. (all sexy like)"
  //   ],
  //   [
  //     "8",
  //     "1 2 3",
  //     "Step towards her while putting left hand over head (all sexy like) and prep for cross body"
  //   ],
  //   ["", "5 6 7", "Cross body with single hand switch across"],
  //   ["9", "1 2 3", "From across to single hand turn"],
  //   ["", "5 6 7", "Finish single hand turn"],
  //   [
  //     "10",
  //     "1 2 3",
  //     "Step back from single hand turn to prep for slow turn to the left"
  //   ],
  //   ["", "5 6 7", "Lead Slow turn to the left (back facing follow)"],
  //   ["11", "1 2 3", "Turn her"],
  //   ["", "5 6 7", "Turn me"],
  //   ["12", "1 2 3", "Turn her"],
  //   ["", "5 6 7", "Mark Time"],
  //   ["13", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body"],
  //   ["14", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body"]
  // ];

  private routine2 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body Open basic"],
    ["2", "1 2 3", "Hands together switch","8"],
    ["", "5 6 7", "Two hand cross body turn"],
    ["3", "1 2 3", "Basic over the top","12"],
    ["", "5 6 7", "Cross body rolling"],
    ["4", "1 2 3", "Basic","15"],
    ["", "5 6 7", "Cross body lead out"],
    ["5", "1 2 3", "Basic","19"],
    ["", "5 6 7", "Cross body lead out"],
    ["6", "1 2 3", "Switch across reach for the hand underneath","22"],
    ["", "5 6 7", "Two hand turn out together"],
    ["7", "1 2 3", "Take it back"],"27",
    ["", "5 6 7", "Mark your step"],
    ["8", "1 2 3", "Basic","31"],
    ["", "5 6 7", "Cross body lead out"],
    ["9", "1 2 3", "Switch across","34"],
    ["", "5 6 7", "Single hand turnout"],
    ["10", "1 2 3", "Take the step back on the back","38"],
    ["", "5 6 7", "Break soft across lead"],
    ["11", "1 2 3", "Take the step back","42"],
    ["", "5 6 7", "Pivot turn. Ladies in place"],
    ["12", "1 2 3", "Take the step back","46"],
    ["", "5 6 7", "Mark your step"],
    ["13", "1 2 3", "Going back to the lead out ","50"],
    ["", "5 6 7", "(Cross body)"],
    ["14", "1 2 3", "(Basic)","53"],
    ["", "5 6 7", "(Cross body)"],
    ["15", "1 2 3", "one two three...","56"],
    ["", "5 6 7", "five six seven..."]
  ];

  // private routine3_OLD = [
  //   ["1", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Rolling turn"],
  //   ["2", "1 2 3", "Catch and approach for cross body"],
  //   ["", "5 6 7", "Cross body and end up in open position"],
  //   ["3", "1 2 3", "Prep for arms together slow turn"],
  //   ["", "5 6 7", "Slow arms together turn and prep for switch across"],
  //   ["4", "1 2 3", "Switch across and guide arm for over the top"],
  //   [
  //     "",
  //     "5 6 7",
  //     "Right hand puts her hand over her head and start turn with left hand"
  //   ],
  //   [
  //     "5",
  //     "1 2 3",
  //     "Catch and go prep for double cross body turn (THE CRUSHER)"
  //   ],
  //   ["", "5 6 7", "Double hand cross body turn"],
  //   ["6", "1 2 3", "Turn into cuddle"],
  //   ["", "5 6 7", "Turn her"],
  //   ["7", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body"],
  //   ["8", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body and prep for switch across"],
  //   ["9", "1 2 3", "Switch across and prep for Single hand turnout"],
  //   ["", "5 6 7", "Single hand lazy half turnout (one arm thingy)"],
  //   [
  //     "10",
  //     "1 2 3",
  //     "Step back and go over her head, set up for fast turn to right"
  //   ],
  //   ["", "5 6 7", "Fast turn to right while maintaining contact with her hand"],
  //   ["11", "1 2 3", "Pull and catch her back shoulder"],
  //   ["", "5 6 7", "Rolling turn"],
  //   ["12", "1 2 3", "Approach for DOUBLE HAND cross body turn"],
  //   ["", "5 6 7", "Double hand Cross body turn"],
  //   ["13", "1 2 3", "Prep for two handed in place turn"],
  //   ["", "5 6 7", "Two handed turn out"],
  //   ["14", "1 2 3", "Step back and two handed over her head"],
  //   ["", "5 6 7", "Turn myself"],
  //   ["15", "1 2 3", "Step back"],
  //   ["", "5 6 7", "Cross body turn"],
  //   ["16", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body"],
  //   ["17", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body"]
  // ];

  private routine3 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body rolling"],
    ["2", "1 2 3", "Basic","7"],
    ["", "5 6 7", "Cross body lead out"],
    ["3", "1 2 3", "Open Basic hands together","11"],
    ["", "5 6 7", "cross body in the open."],
    ["4", "1 2 3", "Switch across","14"],
    ["", "5 6 7", "Across"],
    ["5", "1 2 3", "Basic over the top","18"],
    ["", "5 6 7", "Open hands together cross body turn"],
    ["6", "1 2 3", "Loop 2 3 and","22"],
    ["", "5 6 7", "(cross body turn)"],
    ["7", "1 2 3", "(basic)","26"],
    ["", "5 6 7", "Two cross bodies in the close (cross body)"],
    ["8", "1 2 3", "Basic","29"],
    ["", "5 6 7", "Cross body"],
    ["9", "1 2 3", "Switch across ","33"],
    ["", "5 6 7", "Single hand turnout"],
    ["10", "1 2 3", "Take the step back","37"],
    ["", "5 6 7", "Wrap around turn lead"],
    ["11", "1 2 3", "1 2 3 (pull)","41"],
    ["", "5 6 7", "Pivot turn rolling lead"],
    ["12", "1 2 3", "Basic ","44"],
    ["", "5 6 7", "Cross body lead out"],
    ["13", "1 2 3", "Open Step Pivot","48"],
    ["", "5 6 7", "Two hand turnout"],
    ["14", "1 2 3", "Take the step back","52"],
    ["", "5 6 7", "Pivot turn, ladies in place"],
    ["15", "1 2 3", "123 and... (pull cross body turn)","55"],
    ["", "5 6 7", "(both mark time)"],
    ["16", "1 2 3", "One basic","59"],
    ["", "5 6 7", "Cross body"],
    ["17", "1 2 3", "123 and... (basic)","62"],
    ["", "5 6 7", "(Cross body)"]
  ];

  // private routine4_OLD = [
  //   ["1", "1 2 3", "Approach for cross body"],
  //   ["", "5 6 7", "Cross body and prep for across"],
  //   ["2", "1 2 3", "Across"],
  //   ["", "5 6 7", "Turnout with right hand. SWITCH HANDS"],
  //   [
  //     "3",
  //     "1 2 3",
  //     "Step back… holding her right hand with your left. Prep for big turn"
  //   ],
  //   ["", "5 6 7", "TURN TURN TURN"],
  //   ["4", "1 2 3", "Approach for basic cross body"],
  //   ["", "5 6 7", "Cross body"],
  //   ["5", "1 2 3", "Two hand.. With big prep"],
  //   ["", "5 6 7", "DOUBLE Turn in place with right hand"],
  //   ["6", "1 2 3", "Step to the side, switch hands with right underneath"],
  //   [
  //     "",
  //     "5 6 7",
  //     "Turn out all the way, and prepare to walk under the upraised arms"
  //   ],
  //   [
  //     "7",
  //     "1 2 3",
  //     "Walk through upraised arms and turn her. Put left hand over her head."
  //   ],
  //   [
  //     "",
  //     "5 6 7",
  //     'Turn her around and prep for "alternating arm down" (acrosses)'
  //   ],
  //   [
  //     "8",
  //     "1 2 3",
  //     "Across to the right with her back to you, switch hands and prep for alternating across"
  //   ],
  //   ["", "5 6 7", "Across to the left with her back to you."],
  //   ["9", "1 2 3", "Across to the right with her back to you."],
  //   ["", "5 6 7", "DOUBLE Turnout with the right hand. SWITCH HANDS!"],
  //   ["10", "1 2 3", "Approach for cross body turn"],
  //   ["", "5 6 7", "Cross body"],
  //   ["11", "1 2 3", "Swing out (step back) and pull and catch her"],
  //   ["", "5 6 7", "Rolling turn from catch"],
  //   ["12", "1 2 3", "Approach for basic cross body"],
  //   ["", "5 6 7", "Cross body"]
  // ];

  private routine4 = [
    ["1", "1 2 3", "Basic","2"],
    ["", "5 6 7", "Cross body "],
    ["2", "1 2 3", "switch across to a...","6"],
    ["", "5 6 7", "single hand turn out on..."],
    ["3", "1 2 3", "Take a step back","10"],
    ["", "5 6 7", "(triple turn)"],
    ["4", "1 2 3", "Basic ","14"],
    ["", "5 6 7", "Cross body "],
    ["5", "1 2 3", "Open basic step pivot","17"],
    ["", "5 6 7", "Double turn"],
    ["6", "1 2 3", "Basic hands together down switch... ","20"],
    ["", "5 6 7", "(two-hand) cross body turn"],
    ["7", "1 2 3", "Mark your step lead and","25"],
    ["", "5 6 7", "We’re pulling slightly across"],
    ["8", "1 2 3", "(Across)","28"],
    ["", "5 6 7", "Across"],
    ["9", "1 2 3", "Across","32"],
    ["", "5 6 7", "Double turn"],
    ["10", "1 2 3", "Basic switch to a... ","36"],
    ["", "5 6 7", "cross body swingout"],
    ["11", "1 2 3", "Take the step back","39"],
    ["", "5 6 7", "Pivot turn.ladies rolling lead"],
    ["12", "1 2 3", "Basic","43"],
    ["", "5 6 7", "cross body"],
    ["13", "1 2 3", "Basic","47"],
    ["", "5 6 7", "cross body"],
    ["14", "1 2 3", "Basic","50"],
    ["", "5 6 7", "Complete"]
  ];

  private routine5 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body lead out"],
    ["2", "1 2 3", "Open hands together step pivot","6"],
    ["", "5 6 7", "Single turn out and"],
    ["3", "1 2 3", "(step back and go across)","10"],
    ["", "5 6 7", "Sharp across lead"],
    ["4", "1 2 3", "Switch across","13"],
    ["", "5 6 7", "hands up"],
    ["5", "1 2 3", "Switch back, We’re taking it back","18"],
    ["", "5 6 7", "Switch,  Step back (step around)"],
    ["6", "1 2 3", "And... (pull partner)","22"],
    ["", "5 6 7", "Rolling"],
    ["7", "1 2 3", "Basic two three and...","25"],
    ["", "5 6 7", "(Cross body)"],
    ["8", "1 2 3", "Complete basic and...","29"],
    ["", "5 6 7", "(Cross body)"]
  ];

  private routine6 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body lead out"],
    ["2", "1 2 3", "Switch across","7"],
    ["", "5 6 7", "Its a single turn out, hands together"],
    ["3", "1 2 3", "One Two Three (Lead moves across)","10"],
    ["", "5 6 7", "Pivot turn hands together"],
    ["4", "1 2 3", "Break...left, break right ladies.","14"],
    ["", "5 6 7", "Pivot turn"],
    ["5", "1 2 3", "Break left, break right ladies","18"],
    ["", "5 6 7", "Wrap around turn lead"],
    ["6", "1 2 3", "Take the step back. Pull","22"],
    ["", "5 6 7", "Rolling"],
    ["7", "1 2 3", "Double cross body (basic)","25"],
    ["", "5 6 7", "(cross body)"],
    ["8", "1 2 3", "123 and ... (basic)","29"],
    ["", "5 6 7", "(cross body)"],
    ["9", "1 2 3", "Complete basic","33"],
    ["", "5 6 7", "(finish complete basic)"]
  ];
  private routine7 = [
    ["1", "1 2 3", "(Basic)","3"],
    ["", "5 6 7", "(Cross body) "],
    ["2", "1 2 3", "Switch across","7"],
    ["", "5 6 7", "It’s a two hand turn out"],
    ["3", "1 2 3", "Take the step back","11"],
    ["", "5 6 7", "Pivot turn hands together"],
    ["4", "1 2 3", "To the shoulder, mark your step","14"],
    ["", "5 6 7", "Across"],
    ["5", "1 2 3", "(Across)","18"],
    ["", "5 6 7", "Double turn"],
    ["6", "1 2 3", "Basic switch","22"],
    ["", "5 6 7", "Two hand cross body turn"],
    ["7", "1 2 3", "Basic","25"],
    ["", "5 6 7", "Cross body lead out"],
    ["8", "1 2 3", "Step. Pivot.","29"],
    ["", "5 6 7", "Double turn"],
    ["9", "1 2 3", "Take the step back","33"],
    ["", "5 6 7", "Loop in, ladies in place"],
    ["10", "1 2 3", "Mark your step.","36"],
    ["", "5 6 7", "Across again"],
    ["11", "1 2 3", "Hands up","40"],
    ["", "5 6 7", "(Single turn)"],
    ["12", "1 2 3", "Rock the step back","43"],
    ["", "5 6 7", "We’re going to go around to the turnout and..."],
    ["13", "1 2 3", "(Pull)","47"],
    ["", "5 6 7", "Rolling"],
    ["14", "1 2 3", "Basic","50"],
    ["", "5 6 7", "Cross body"],
    ["15", "1 2 3", "Complete basic","54"],
    ["", "5 6 7", "(Finish basic)"]
  ];
  private routine8 = [
    ["1", "1 2 3", "Basic","2"],
    ["", "5 6 7", "Cross body"],
    ["2", "1 2 3", "Switch across","6"],
    ["", "5 6 7", "Double turn"],
    ["3", "1 2 3", "Basic switch","10"],
    ["", "5 6 7", "Cross body swivel in"],
    ["4", "1 2 3", "Across","14"],
    ["", "5 6 7", "Across"],
    ["5", "1 2 3", "Turn out basic","17"],
    ["", "5 6 7", "Pull, cross body"],
    ["6", "1 2 3", "Step. Pivot.","21"],
    ["", "5 6 7", "Double turn"],
    ["7", "1 2 3", "Basic switch down","24"],
    ["", "5 6 7", "Cross body turn"],
    ["8", "1 2 3", "Mark your step lead","28"],
    ["", "5 6 7", "We’re slightly pulling lead"],
    ["9", "1 2 3", "We’re marking our step again","32"],
    ["", "5 6 7", "We’re slightly pulling"],
    ["10", "1 2 3", "Across","36"],
    ["", "5 6 7", "Across"],
    ["11", "1 2 3", "Over the top","39"],
    ["", "5 6 7", "Pull and roll"],
    ["12", "1 2 3", "Basic over the top","43"],
    ["", "5 6 7", "Cross body rolling"],
    ["13", "1 2 3", "One two three (basic)","47"],
    ["", "5 6 7", "(Cross body)"],
    ["14", "1 2 3", "Complete basic","50"],
    ["", "5 6 7", "(Finish basic)"]
  ];
  private routine9 = [
    ["1", "1 2 3", "Basic","2"],
    ["", "5 6 7", "Cross body"],
    ["2", "1 2 3", "Across","6"],
    ["", "5 6 7", "Two hand turn out"],
    ["3", "1 2 3", "Take it back","9"],
    ["", "5 6 7", "Over the top, together"],
    ["4", "1 2 3", "Take the step back","13"],
    ["", "5 6 7", "Rolling"],
    ["5", "1 2 3", "Basic","17"],
    ["", "5 6 7", "Cross body lead out"],
    ["6", "1 2 3", "Switch across","20"],
    ["", "5 6 7", "Turn out"],
    ["7", "1 2 3", "Take the step back","24"],
    ["", "5 6 7", "Hook on"],
    ["8", "1 2 3", "Sharp","28"],
    ["", "5 6 7", "Step. Pivot. Double turn"],
    ["9", "1 2 3", "Basic hands together down","31"],
    ["", "5 6 7", "Two hand cross body turn"],
    ["10", "1 2 3", "We’re Looping in","35"],
    ["", "5 6 7", "Combination"],
    ["11", "1 2 3", "Basic over the top","39"],
    ["", "5 6 7", "Cross body rolling"],
    ["12", "1 2 3", "Basic","42"],
    ["", "5 6 7", "Cross body lead out"],
    ["13", "1 2 3", "Basic","46"],
    ["", "5 6 7", "Cross body lead out"],
    ["14", "1 2 3", "It’s a complete basic","50"],
    ["", "5 6 7", "(finish basic)"]
  ];
  private routine10 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body lead out"],
    ["2", "1 2 3", "Switch across","6"],
    ["", "5 6 7", "Double turn"],
    ["3", "1 2 3", "Basic switch","10"],
    ["", "5 6 7", "Cross body half turn in"],
    ["4", "1 2 3", "You’re pulling back","13"],
    ["", "5 6 7", "Push"],
    ["5", "1 2 3", "Step. Pivot ","17"],
    ["", "5 6 7", "Turn out"],
    ["6", "1 2 3", "Take the step back","20"],
    ["", "5 6 7", "(Double turn)"],
    ["7", "1 2 3", "Open shuffle","24"],
    ["", "5 6 7", "(turn her back towards you)"],
    ["8", "1 2 3", "Quarter across unlock","28"],
    ["", "5 6 7", "Back to the open shuffle on the turn and a half"],
    ["9", "1 2 3", "(Shuffle)","31"],
    ["", "5 6 7", "Hands together"],
    ["10", "1 2 3", "Step. Pivot two three","35"],
    ["", "5 6 7", "Rock the back step"],
    ["11", "1 2 3", "Hands together","39"],
    ["", "5 6 7", "We’re hooking in"],
    ["12", "1 2 3", "One two three and... (go around again)","42"],
    ["", "5 6 7", "(Hooking in again)"],
    ["13", "1 2 3", "One two three and... (pull)","46"],
    ["", "5 6 7", "(Double turn)"],
    ["14", "1 2 3", "Basic over the top","49"],
    ["", "5 6 7", "Open hands together (cross body turn)"],
    ["15", "1 2 3", "Loop 23 and...","53"],
    ["", "5 6 7", "(Single turn)"],
    ["16", "1 2 3", "Basic over the top","57"],
    ["", "5 6 7", "Cross body rolling"],
    ["17", "1 2 3", "Basic","60"],
    ["", "5 6 7", "Cross body lead out"],
    ["18", "1 2 3", "Basic complete","64"],
    ["", "5 6 7", "(Finish basic)"]
  ];
  private routine11 = [
    ["1", "1 2 3", "Basic","2"],
    ["", "5 6 7", "Cross body lead out"],
    ["2", "1 2 3", "Single hand step pivot","6"],
    ["", "5 6 7", "Turn out"],
    ["3", "1 2 3", "Drop over to the top","10"],
    ["", "5 6 7", "Hands"],
    ["4", "1 2 3", "Throw it up","14"],
    ["", "5 6 7", "And... curl her in"],
    ["5", "1 2 3", "We're pushing back","17"],
    ["", "5 6 7", "Spin turn"],
    ["6", "1 2 3", "Tap","21"],
    ["", "5 6 7", "To the hip"],
    ["7", "1 2 3", "Basic","24"],
    ["", "5 6 7", "Cross body"],
    ["8", "1 2 3", "Basic","28"],
    ["", "5 6 7", "Cross body"],
    ["9", "1 2 3", "And it's a complete basic","31"],
    ["", "5 6 7", "Five six seven (Finish basic)"]
  ];
  private routine12 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body lead out"],
    ["2", "1 2 3", "Take a step back","6"],
    ["", "5 6 7", "Women go around and..."],
    ["3", "1 2 3", "(Continue around)","10"],
    ["", "5 6 7", "Hard five six seven (continue around)"],
    ["4", "1 2 3", "TWO AND THREE","13"],
    ["", "5 6 7", "(cross body)"],
    ["5", "1 2 3", "Switch across","16"],
    ["", "5 6 7", "Double turn"],
    ["6", "1 2 3", "Take a step back","20"],
    ["", "5 6 7", "Wrap around turn"],
    ["7", "1 2 3", "Reach for the hand underneath, we're pulling","23"],
    ["", "5 6 7", "Across"],
    ["8", "1 2 3", "Quarter two hand turn out","27"],
    ["", "5 6 7", "To a quarter lock"],
    ["9", "1 2 3", "(Hands over the head)","31"],
    ["", "5 6 7", "Turn and a half"],
    ["10", "1 2 3", "Open shuffle","34"],
    ["", "5 6 7", "Quarter across"],
    ["11", "1 2 3", "To a lock","38"],
    ["", "5 6 7", "Double turn"],
    ["12", "1 2 3", "Basic hands together","41"],
    ["", "5 6 7", "Turn out"],
    ["13", "1 2 3", "Tap","44"],
    ["", "5 6 7", "Turn  "],
    ["14", "1 2 3", "(Twist her and both turn out)","48"],
    ["", "5 6 7", "Five six seven and (Lead turns)"],
    ["15", "1 2 3", "(Basic)","51"],
    ["", "5 6 7", "Five six seven (Cross body)"],
    ["16", "1 2 3", "(Basic)","55"],
    ["", "5 6 7", "Cross body"],
    ["17", "1 2 3", "One two three (basic)","58"],
    ["", "5 6 7", "(Finish Basic)"]
  ];

  private routine13 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body"],
    ["2", "1 2 3", "Switch Across","6"],
    ["", "5 6 7", "Double turn hands together"],
    ["3", "1 2 3", "Take a step back","10"],
    ["", "5 6 7", "(Lead turns)"],
    ["4", "1 2 3", "Switch down","13"],
    ["", "5 6 7", "Switch up"],
    ["5", "1 2 3", "Switch down","16"],
    ["", "5 6 7", "Switch up"],
    ["6", "1 2 3", "Switch back and pull","20"],
    ["", "5 6 7", "(Go around)"],
    ["7", "1 2 3", "curl her in","23"],
    ["", "5 6 7", "pivot"],
    ["8", "1 2 3", "step. pivot.","27"],
    ["", "5 6 7", "turn out"],
    ["9", "1 2 3", "Take the step back","30"],
    ["", "5 6 7", "double turn basic hands down"],
    ["10", "1 2 3", "To a cross body","33"],
    ["", "5 6 7", "pull"],
    ["11", "1 2 3", "step. pivot.","36"],
    ["", "5 6 7", "turn in a quarter"],
    ["12", "1 2 3", "we're pushing out","40"],
    ["", "5 6 7", "we're pulling"],
    ["13", "1 2 3", "Take a step back ladies, loop lead","43"],
    ["", "5 6 7", "(cross body turn)"],
    ["14", "1 2 3", "we're switching up","47"],
    ["", "5 6 7", "(switch across)"],
    ["15", "1 2 3", "And we're going to a long quarter across","50"],
    ["", "5 6 7", "(Turn her and throw hand up)"],
    ["16", "1 2 3", "pivot (begin double turn)","54"],
    ["", "5 6 7", "(End double turn)"],
    ["17", "1 2 3", "Basic hands together ","57"],
    ["", "5 6 7", "turn out "],
    ["18", "1 2 3", "Basic ","60"],
    ["", "5 6 7", "cross body lead out"],
    ["19", "1 2 3", "Basic ","63"],
    ["", "5 6 7", "cross body lead out"],
    ["20", "1 2 3", "Complete basic","67"],
    ["", "5 6 7", "Five six seven (finish basic)"]
  ];

  private routine14 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body lead out"],
    ["2", "1 2 3", "Open Hands together, we're pulling even","6"],
    ["", "5 6 7", "Pushing"],
    ["3", "1 2 3", "Back Pivoting","10"],
    ["", "5 6 7", "Turn out"],
    ["4", "1 2 3", "Drop and pull","13"],
    ["", "5 6 7", "Hands together"],
    ["5", "1 2 3", "Half turn in","16"],
    ["", "5 6 7", "Double turn "],
    ["6", "1 2 3", "Basic over the top","20"],
    ["", "5 6 7", "Soft cross body turn"],
    ["7", "1 2 3", "Switch","23"],
    ["", "5 6 7", "Spin turn"],
    ["8", "1 2 3", "Catch at the shoulder","27"],
    ["", "5 6 7", "To a rolling"],
    ["9", "1 2 3", "Basic","30"],
    ["", "5 6 7", "Cross body   "],
    ["10", "1 2 3", "Switch Across (grab other hand)","33"],
    ["", "5 6 7", "Double turn "],
    ["11", "1 2 3", "Take a step back","37"],
    ["", "5 6 7", "Going over the top (both)"],
    ["12", "1 2 3", "Slightly pulling with the right hand.","40"],
    ["", "5 6 7", "Ladies in place, pivot turn"],
    ["13", "1 2 3", "Half spin turn in","44"],
    ["", "5 6 7", "(men come around)"],
    ["14", "1 2 3", "Across","47"],
    ["", "5 6 7", "Double turn"],
    ["15", "1 2 3", "Basic hands together to a","51"],
    ["", "5 6 7", "two hand cross body turn"],
    ["16", "1 2 3", "Basic  ","54"],
    ["", "5 6 7", "Cross body lead out"],
    ["17", "1 2 3", "Step pivot","57"],
    ["", "5 6 7", "Double turn"],
    ["18", "1 2 3", "Basic","61"],
    ["", "5 6 7", "Cross body turn again"],
    ["19", "1 2 3", "Pivot turn half","64"],
    ["", "5 6 7", "(face her away into shuffle)"],
    ["20", "1 2 3", "Across (shuffle)","68"],
    ["", "5 6 7", "Over the top"],
    ["21", "1 2 3", "Pull","72"],
    ["", "5 6 7", "And roll"],
    ["22", "1 2 3", "Switch. (Lead walks under arms)","75"],
    ["", "5 6 7", "Spin out turn"],
    ["23", "1 2 3", "Basic","78"],
    ["", "5 6 7", "Cross body lead out"],
    ["24", "1 2 3", "Basic ","82"],
    ["", "5 6 7", "Cross body lead out"],
    ["25", "1 2 3", "Complete basic and","95"],
    ["", "5 6 7", "Five six seven (finish basic)"]
  ];
  private routine15 = [
    ["1", "1 2 3", "Basic","3"],
    ["", "5 6 7", "Cross body lead out. Open"],
    ["2", "1 2 3", "Step. Pivot","7"],
    ["", "5 6 7", "Turn out"],
    ["3", "1 2 3", "Basic","10"],
    ["", "5 6 7", "Half cross body turn in to the shoulder"],
    ["4", "1 2 3", "Pulling her back","13"],
    ["", "5 6 7", "Double turn "],
    ["5", "1 2 3", "Basic","17"],
    ["", "5 6 7", "Cross body lead out."],
    ["6", "1 2 3", "Quarter","20"],
    ["", "5 6 7", "Hands together (turn her and 1.5 until she faces away)"],
    ["7", "1 2 3", "Open Shuffle.","23"],
    ["", "5 6 7", "(Double turn and hammer lock)"],
    ["8", "1 2 3", "And.. (Basic in hammer lock)","27"],
    ["", "5 6 7", "Cross body. (come out of hammer lock)"],
    ["9", "1 2 3", "Pull","30"],
    ["", "5 6 7", "Over the shoulder (both)"],
    ["10", "1 2 3", "Pull","34"],
    ["", "5 6 7", "Over the shoulder"],
    ["11", "1 2 3", "One two three and (tap)","37"],
    ["", "5 6 7", "(Hip and lead turns)"],
    ["12", "1 2 3", "One two three and (Catch by shoulder and turn out)","41"],
    ["", "5 6 7", "(double turn out)"],
    ["13", "1 2 3", "Basic ","44"],
    ["", "5 6 7", "Cross body"],
    ["14", "1 2 3", "Basic ","47"],
    ["", "5 6 7", "Cross body"],
    ["15", "1 2 3", "Complete basic","51"],
    ["", "5 6 7", "five six seven (finish basic)"]
  ];

  private path: any;
  private debug: boolean = false;

  private routine: any;
  private tableguts = [];
  private selectedView: String = "-oldCam_1";
  private currentView: String = "-oldCam_1";
  private views = ["-oldCam_1", "-android_1", "-iphone_1", "-overhead_1"];
  private server = "http://34.217.224.28/media/salsa/";

  private videoURL: any;
  private showVids: boolean = false;
  private oldvid: HTMLVideoElement = null;
  private newvid: HTMLVideoElement = <HTMLVideoElement>document.getElementById(
    this.views[0]
  );
  private vid1: HTMLVideoElement = null;
  private vid2: HTMLVideoElement = null;
  private vid3: HTMLVideoElement = null;
  private vid4: HTMLVideoElement = null;

  private showAuds: boolean = false;
  private audioURL: any;

  private showStream = false;
  private vidStreamURL = null;
  private audStreamURL = null;

  private name = null;

  ////////////////////////////////////////////
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public filex: File,
    public transfer: FileTransfer,
    private toastCtrl: ToastController
  ) {
    ////////////////////////////////////////////
    ////////// C O N S T R U C T O R ///////////
    ////////////////////////////////////////////

    // Initialize variables:
    this.routine = this.navParams.get("userParams");
    if (this.debug) {
      alert("userParams:" + this.routine);
    }
    // Get name and number
    this.name = this.routine.split("R").join(" R");

    this.path = this.filex.dataDirectory + "files/";
    this.videoURL = [];
    this.audioURL = [];

    // this is used if the files are not already loaded.  the program will just show one view.
    this.vidStreamURL =
      this.server + this.routine + this.views[0] + ".mp4#t=0.1";
    this.audStreamURL = this.server + this.routine + ".mp3";

    // Go see what files we need and download them
    this.getAndDisplayVidFiles();
    this.getAndDisplayAudFiles();
  }
  //______________________________________________________________________________//
  ionViewDidLoad() {
    console.log("ionViewDidLoad Video4Page");
    if (this.routine == "1stRoutine") {
      this.tableguts = this.routine1;
    } else if (this.routine == "2ndRoutine") {
      this.tableguts = this.routine2;
    } else if (this.routine == "3rdRoutine") {
      this.tableguts = this.routine3;
    } else if (this.routine == "4thRoutine") {
      this.tableguts = this.routine4;
    } else if (this.routine == "5thRoutine") {
      this.tableguts = this.routine5;
    } else if (this.routine == "6thRoutine") {
      this.tableguts = this.routine6;
    } else if (this.routine == "7thRoutine") {
      this.tableguts = this.routine7;
    } else if (this.routine == "8thRoutine") {
      this.tableguts = this.routine8;
    } else if (this.routine == "9thRoutine") {
      this.tableguts = this.routine9;
    } else if (this.routine == "10thRoutine") {
      this.tableguts = this.routine10;
    } else if (this.routine == "11thRoutine") {
      this.tableguts = this.routine11;
    } else if (this.routine == "12thRoutine") {
      this.tableguts = this.routine12;
    } else if (this.routine == "13thRoutine") {
      this.tableguts = this.routine13;
    } else if (this.routine == "14thRoutine") {
      this.tableguts = this.routine14;
    } else if (this.routine == "15thRoutine") {
      this.tableguts = this.routine15;
    }
  }

  //______________________________________________________________________________//
  async getAndDisplayVidFiles() {
    for (var i = 0; i < this.views.length; i++) {
      let view = this.views[i];

      try {
        await this.filex.checkFile(   // let beer = 
          this.path,
          this.routine + view + ".mp4"
        );
        if (this.debug) {
          alert("file exists " + view);
        }
        let file = this.path + this.routine + view + ".mp4#t=0.1";
        file = file.replace(/file:\/\//g, "");
        this.videoURL.push(file);
      } catch (err) {
        if (this.debug) {
          alert("file does not exist: " + view);
        }
        let localPath = this.path + this.routine + view + ".mp4";
        let name = this.routine + view + ".mp4";
        this.downloadMP4(localPath, name);
      }
    } //end of for loop that steps through views.
    this.displayVideo(); //(this has to be in here instead of the constructor because of "async")
  }

  //______________________________________________________________________________//
  displayVideo() {
    if (this.debug) {
      alert("videoURL length = " + this.videoURL.length);
    }
    if (this.videoURL.length > 2) {
      this.showVids = true;
      this.showStream = false;
      this.newvid = <HTMLVideoElement>document.getElementById(this.views[0]); //for speed changes before camera swap
    } else {
      this.showVids = false;
      this.showStream = true;
    }
  }

  //______________________________________________________________________________//
  async getAndDisplayAudFiles() {
    try {
      await this.filex.checkFile(this.path, this.routine + ".mp3");    //let beer = 
      if (this.debug) {
        alert("audio exists");
      }
      let file = this.path + this.routine + ".mp3";
      file = file.replace(/file:\/\//g, "");
      this.audioURL.push(file);
    } catch (err) {
      if (this.debug) {
        alert("audio does not exist");
      }
      let localPath = this.path + this.routine + ".mp3";
      let name = this.routine + ".mp3";
      this.downloadMP3(localPath, name);
    }

    this.displayAudio();
  }

  //______________________________________________________________________________//
  displayAudio() {
    this.showAuds = !this.showAuds;
  }

  //______________________________________________________________________________//
  clicked_table() {
    console.log("Show a full page write-up");
    this.navCtrl.push("TablePage", {
      name: this.name,
      tableguts: this.tableguts
    });
  }

  //______________________________________________________________________________//
  downloadMP4(localPath, name) {
    //let alteredRoutine = this.routine.replace(/ /g, "%20");
    let url = this.server + name;
    if (this.debug) {
      alert("downloading video..." + name);
      alert("url: " + url);
    }
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(url, localPath).then(
      entry => {
        if (this.debug) {
          alert(JSON.stringify(entry));
        }
      },
      err => {
        console.log("Error", err);
      }
    );
  }

  //______________________________________________________________________________//
  downloadMP3(localPath, name) {
    let url = this.server + name;
    if (this.debug) {
      alert("downloading mp3... ");
      alert("url: " + url);
    }
    const fileTransfer: FileTransferObject = this.transfer.create();
    fileTransfer.download(url, this.path + this.routine + ".mp3").then(
      entry => {
        if (this.debug) {
          alert(JSON.stringify(entry));
        }
      },
      err => {
        // handle error
        console.log("Error", err);
      }
    );
  }

  //______________________________________________________________________________//
  // changeView(ev) {
  //   //this method is not being used since I decided to go with buttons instead of the ion select.. but I thought I'd
  //   //leave it here anyway.
  //   //alert("event: "+ ev )  this just gives the value of the selection -oldCam_1
  //   let vid1 = <HTMLVideoElement>document.getElementById("-oldCam_1");
  //   let vid2 = <HTMLVideoElement>document.getElementById("-android_1");
  //   let vid3 = <HTMLVideoElement>document.getElementById("-iphone_1");
  //   let vid4 = <HTMLVideoElement>document.getElementById("-overhead_1");

  //   //get controls for old vid view:
  //   if (this.currentView === this.views[0]) {
  //     this.oldvid = vid1;
  //   }
  //   if (this.currentView === this.views[1]) {
  //     this.oldvid = vid2;
  //   }
  //   if (this.currentView === this.views[2]) {
  //     this.oldvid = vid3;
  //   }
  //   if (this.currentView === this.views[3]) {
  //     this.oldvid = vid4;
  //   }

  //   //get controls for new vid view
  //   if (ev === this.views[0]) {
  //     this.newvid = vid1;
  //   }
  //   if (ev === this.views[1]) {
  //     this.newvid = vid2;
  //   }
  //   if (ev === this.views[2]) {
  //     this.newvid = vid3;
  //   }
  //   if (ev === this.views[3]) {
  //     this.newvid = vid4;
  //   }

  //   if (this.oldvid.paused) {
  //     this.newvid.currentTime = this.oldvid.currentTime;
  //   } else {
  //     this.oldvid.pause();
  //     this.newvid.currentTime = this.oldvid.currentTime;
  //     this.newvid.play();
  //   }
  //   this.currentView = this.selectedView;
  // }

  //______________________________________________________________________________//
  buttonView(ev) {
    //alert("event: "+ ev )  this just gives the value of the selection -oldCam_1
    this.vid1 = <HTMLVideoElement>document.getElementById(this.views[0]);
    this.vid2 = <HTMLVideoElement>document.getElementById(this.views[1]);
    this.vid3 = <HTMLVideoElement>document.getElementById(this.views[2]);
    this.vid4 = <HTMLVideoElement>document.getElementById(this.views[3]);

    //get controls for old vid player:
    if (this.currentView === this.views[0]) {
      this.oldvid = this.vid1;
    }
    if (this.currentView === this.views[1]) {
      this.oldvid = this.vid2;
    }
    if (this.currentView === this.views[2]) {
      this.oldvid = this.vid3;
    }
    if (this.currentView === this.views[3]) {
      this.oldvid = this.vid4;
    }

    //get controls for new vid player
    if (ev === this.views[0]) {
      this.newvid = this.vid1;
    }
    if (ev === this.views[1]) {
      this.newvid = this.vid2;
    }
    if (ev === this.views[2]) {
      this.newvid = this.vid3;
    }
    if (ev === this.views[3]) {
      this.newvid = this.vid4;
    }

    if (this.oldvid.paused) {
      this.newvid.currentTime = this.oldvid.currentTime;
    } else {
      this.oldvid.pause();
      this.newvid.currentTime = this.oldvid.currentTime;
      this.newvid.play();
    }

    this.selectedView = ev;

    this.currentView = this.selectedView;
  }

  //______________________________________________________________________________//

  toggleShowVids() {
    this.showVids = !this.showVids;
    this.newvid = this.vid1;
  }

  vidSpeedUp() {
    let vid = <HTMLVideoElement>document.getElementById("stream");
    vid.playbackRate = vid.playbackRate + 0.1;
  }

  vidSpeedDown() {
    let vid = <HTMLVideoElement>document.getElementById("stream");
    let rate = vid.playbackRate - 0.1;
    if (rate < 0.1 && rate > -0.1) {
      rate = 0.1;
    }
    vid.playbackRate = rate;
  }

  vidSpeedUpSaved() {
    if (this.newvid === null) {
      this.newvid = <HTMLVideoElement>document.getElementById(this.views[0]);
      this.vid1 = <HTMLVideoElement>document.getElementById(this.views[0]);
      this.vid2 = <HTMLVideoElement>document.getElementById(this.views[1]);
      this.vid3 = <HTMLVideoElement>document.getElementById(this.views[2]);
      this.vid4 = <HTMLVideoElement>document.getElementById(this.views[3]);
    }
    let rate = this.newvid.playbackRate + 0.1;
    this.newvid.playbackRate = rate;
    this.vid1.playbackRate = rate;
    this.vid2.playbackRate = rate;
    this.vid3.playbackRate = rate;
    this.vid4.playbackRate = rate;

    this.presentVideoToast(rate)
  }

  vidSpeedDownSaved() {
    if (this.newvid === null) {
      this.newvid = <HTMLVideoElement>document.getElementById(this.views[0]);
      this.vid1 = <HTMLVideoElement>document.getElementById(this.views[0]);
      this.vid2 = <HTMLVideoElement>document.getElementById(this.views[1]);
      this.vid3 = <HTMLVideoElement>document.getElementById(this.views[2]);
      this.vid4 = <HTMLVideoElement>document.getElementById(this.views[3]);
    }
    let rate = this.newvid.playbackRate - 0.1;
    if (rate < 0.1 && rate > -0.1) {
      rate = 0.1;
    }
    this.newvid.playbackRate = rate;
    this.vid1.playbackRate = rate;
    this.vid2.playbackRate = rate;
    this.vid3.playbackRate = rate;
    this.vid4.playbackRate = rate;
    this.presentVideoToast(rate)
  }

  audSpeedUp() {
    let aud = <HTMLAudioElement>document.getElementById("audio");
    aud.playbackRate = aud.playbackRate + 0.02;
    this.presentAudioToast(aud.playbackRate);
  }

  audSpeedDown() {
    let aud = <HTMLAudioElement>document.getElementById("audio");
    let rate = aud.playbackRate - 0.02;
    if (rate < 0.1 && rate > -0.1) {
      rate = 0.1;
    }
    aud.playbackRate = rate;
    this.presentAudioToast(aud.playbackRate);

  }

  updateNewvid() {
    this.newvid = <HTMLVideoElement>document.getElementById("-oldCam_1");
  }

  presentVideoToast(rate) {
    rate = Math.round(rate * 100) / 100
    let toast = this.toastCtrl.create({
      message: 'Video playback speed changed to: '+ rate,
      duration: 1000,
      position: 'bottom'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentAudioToast(rate) {
    rate = Math.round(rate * 100) / 100
    let toast = this.toastCtrl.create({
      message: 'Audio playback speed changed to: '+ rate,
      duration: 1000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  skipVid(time){
    this.vid1.currentTime = time;
    this.vid2.currentTime = time;
    this.vid3.currentTime = time;
    this.vid4.currentTime = time;
    this.newvid.currentTime = time;
  }
}
