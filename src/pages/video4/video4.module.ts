import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Video4Page } from './video4';

@NgModule({
  declarations: [
    Video4Page,
  ],
  imports: [
    IonicPageModule.forChild(Video4Page),
  ],
})
export class Video4PageModule {}
