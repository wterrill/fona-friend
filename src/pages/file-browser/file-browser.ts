import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular'
import { File } from '@ionic-native/file'
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer'; //FileUploadOptions

/**
 * Generated class for the FileBrowserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
//declare var window

@IonicPage()
@Component({
  selector: 'page-file-browser',
  templateUrl: 'file-browser.html',
})
export class FileBrowserPage {

  ////////////////////////////////////////////
  ///// G L O B A L    V A R I A B L E S////// 
  ////////////////////////////////////////////
  public fileList: any;
  public path:any;
  public videoURL:any;
  private debug: boolean = false;
  public loaded: boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public filex: File,
    public transfer: FileTransfer
  ) {

  }
//______________________________________________________________________________//
  ionViewDidLoad() {
    console.log('ionViewDidLoad FileBrowserPage');
    this.getURLS();
  }

//______________________________________________________________________________//
  getURLS(){
    this.path = this.filex.dataDirectory + "files/";
    this.videoURL = null;
  
    if(this.debug){alert("videoURL");}
    this.filex.checkFile(this.path,"1st Routine.mp4").then((result)=>
      {
        if(this.debug){
          alert("file exists")
          alert(JSON.stringify(result));
        }
        this.videoURL = this.path+'1st Routine.mp4';
        this.videoURL = this.videoURL.replace(/file:\/\//g, "")
        this.loaded = true;
      }).catch((err)=>{
        if(this.debug){
          alert("file does not exist")
          alert(JSON.stringify(err));
        }
        this.videoURL = "http://34.217.224.28/media/salsa/1st%20Routine.mp4"
        this.loaded = true;
        this.downloadMP4()
      });
  
    this.getFileList();
  }

//______________________________________________________________________________//  
  writeCSV(){
    this.filex.writeFile(this.path, 'test.csv', 'hello,world,', {replace: true})
     .then(() => {      
       console.log('ok. yup. Wh!!!!');
       this.getFileList();
     })
     .catch((err) => {
       console.error(err);
     });

 }

//______________________________________________________________________________//
 toggle(){
   if(this.loaded === true){
     this.loaded = false
   } else {
     this.loaded = true
   }
 }

//______________________________________________________________________________// 
 downloadMP4(){
   let url = "http://34.217.224.28/media/salsa/1st%20Routine.mp4"
     const fileTransfer: FileTransferObject = this.transfer.create();
     fileTransfer.download(url, this.path+'1st Routine.mp4').then((entry) => {
      if(this.debug){
        alert("downloading mp3... this will take some time.");
        alert('download complete: ' + entry.toURL());
        alert(JSON.stringify(entry))}
      this.getFileList();
    }, (err) => {
      // handle error
      alert(JSON.stringify(err));
      console.log('Error',err);
    });
 }

//______________________________________________________________________________//
 deletefile(file){
  this.filex.removeFile(this.path, file).then((result)=>
  {
    if(this.debug){
      alert("file deleted");
      alert(JSON.stringify(result))
    }
    this.getFileList();
  })
 }

//______________________________________________________________________________//
 getFileList()
 {
  this.filex.listDir(this.path, '').then(
    (files: any) => 
    {
      this.fileList = files;
      if(this.debug)
      {
        alert("getFilesList");
        alert(JSON.stringify(files[0]));
        alert(JSON.stringify(files[0].nativeURL));
        alert(JSON.stringify(this.filex.dataDirectory))
        alert(this.path)
        alert(this.videoURL);
      }

      // TODO I'd like to get this code working so that I can show the file size in the file browser.
      // files.forEach((file) => 
      // {
        
      //   window.resolveLocalFileSystemUrl(file.nativeURL, (fileEntry) => {

      //       fileEntry.getMetadata((metadata) => {
      //           alert("image size : " + metadata.size);
      //           alert("image date : " + metadata.modificationTime);
      //       });

      //   }, (error) => { alert(error);
      //     console.error(error); });
      // });
    }
  ).catch
        (
          (err) => {
            if(this.debug)
            {
              alert("catch for getFileList")
              alert(JSON.stringify(err));
            }
          }
        );
  }


}
